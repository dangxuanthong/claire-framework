##Claire v2 là một framework Javascript chuyên dùng cho API server.

- được viết bằng TypeScript
- hỗ trợ websocket và HTTP request
- hỗ trợ CLI để generate base project (claire-cli)


#### Change Log:

2.3.5 [Breaking change]
- Change DataTable decorator signature
- Change DataField decorator signature
 
2.3.4
- Allow DefaultSqlAdapter to omit migration

2.3.2
- DefaultEnvProvider parses simple env without env prefix

2.3.1
- fix DefaultStaticFileController

2.3.0 [Breaking change]
- fix request upgrader, merge handlers
- add new controller metadata interface, change HttpHandlerMetadata to ControllerHandlerMetadata

2.2.1
- fix request handler lost context when calling in lambda function, leads to wrong query, params and body passing

2.2.0 [Breaking change]
- Fix handler query, params, body async resolve
- Add @PermissionGroup for controller, modify @Permission for handler
- Reorder @Validator parameters

2.1.13
- refactor transaction
- DefaultHttpResourceController skips skipped field in model when createResource and updateResource
- fix ModelMetadata dependency sort

2.1.12
- fix transaction bug in DefaultSqlAdapter, DefaultNoSqlAdapter

2.1.11 [Breaking change]
- remove env provider from AppContext. We recommend to fully configure the app with env provider before running it. 

2.1.10
- add warning when override controller route
- fix DefaultEnvProvider parsing boolean
- add CORS and body parser default implementation
- add transaction support in database adapter

2.1.9
- fix DefaultHttpResourceController missing case for array value in getAllResources

2.1.8
- fix DefaultNoSqlAdapter requires primary key when autoInc with number data type (again, wtf)

2.1.7
- fix DefaultNoSqlAdapter requires primary key when autoInc with number data type
- populate autoInc primary key when insert rather than when get

2.1.6
- fix DefaultSqlAdapter updateMany return 0 when updated value matches found value

2.1.5
- default HttpResourceController getAllResources has query operator accepts primary key and foreign key query as an array of ids
- default HttpResourceController getAllResources has query operator accepts string key as regular expression
- add new regex operator for database adapter

2.1.4
- fix Boolean converting in DefaultDataValidator, missing data type in DefaultHttpResourceController

2.1 [Breaking change]
- FileLogMedia rotation by number of day
- remove using of callback in ClaireBuilder
- add implementation to almost all abstract classes and provider AppContext in init function
- fix absolute sequelize-cli call
- Controller: @OpenAccess decorator to by pass AuthorizationProvider
- DefaultEnvProvider: add fileNameResolver
- add SubSet DataValidationRule
- add query validation for DefaultHttpResourceController
- add RBAC and IAcccessCondition, ConditionValueType
- fix DefaultApiDocController exception when getting mount points
- Modify initable interface to include stop 
- Fix not bootstrap bug
- Add stoppable to Initable interface

2.0 [Breaking change]
- refactor set request handler
- abstract DefaultMongoDBAdapter to DefaultNoSqlAdapter
- rename Ws to Socket for all classes
- add DefaultApiDocController for exposing api document
- allow undefined in log media, controller and service array

1.9.0 [Breaking change]
- change HTTP controller decorator to Mapping
- add request params validation and data type parsing
- improve log readability
- fix bug of pre-using model -> injection of models
- adding log of HTTP method in DefaultHttpErrorHandler
- fix saveOne/saveMany does not populate default value
- fix findOne not check for primaryType for auto id

1.8.0 [Breaking change]
- refactor WsChannelHandler, using Message decorator
- update dependencies and test

1.7.0 [Breaking change] 
- getEnvProvider.load receive class argument instead of generic argument
- claire.start return Promise<IAppContext> and throw ClaireError
- fix bug: claire.start does not require argument

1.6 [Breaking change]
- refactor IQueryProvider and IQuery
- model does no longer need of constructor at definition

1.5 [Breaking change]
- remove TableMapper Encoding decorator
- change in DefaultEnvProvider: now using EnvTemplate decorator and EnvVar decorator
- remove operatorAliases in sequelize option

1.4 [Breaking change]
- getOne does no more throw Exception. F\*ck you getOne. F\*ck me too.

1.3 [Breaking change]
- Initable init function return Promise<void>
- add boolean type to QueryOperator

1.2 [Breaking change]
- remove HttpRouteHandler and getRoutes in AbstractHttpController, using decorator instead
- remove controller implementation of Initable
- DefaultStaticController has mount path of "/static"
- fix HttpRouteHandler not init prototype

1.1
- remove mandatory of responder, middleware, requestValidator, responseValidator in HttpRouteHandler
- remove generic from IHttpRequest
- add string validation for DefaultDataValidator
- add strictTypeCheck option to check HTTP request params (no-strict) and request body (strict)
- add projection option to getOne and getMany


















