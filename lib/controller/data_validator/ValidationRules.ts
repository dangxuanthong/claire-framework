export const PROTOTYPE_VALIDATION_FIELD = "_class_validation_prototype";

export class ValidationFieldMetadata {
    required?: boolean;
    inRange?: { from: number, to: number };
    inSet?: any[];
    match?: string;
    isInteger?: boolean;
    greaterThan?: number;
    lessThan?: number;
    dataType?: string;
    defaultValue?: boolean | string | number;
    isArray?: boolean;
    allowEmpty?: boolean;
    subSet?: any[];
}

export class ValidationMetadata {
    public needValidate: boolean;
    public readonly fields: { [fieldName: string]: ValidationFieldMetadata };

    public constructor() {
        this.needValidate = false;
        this.fields = {};
    }
}

const init_class_validation_prototype = (prototype: any): ValidationMetadata => {
    if (!prototype[PROTOTYPE_VALIDATION_FIELD]) {
        prototype[PROTOTYPE_VALIDATION_FIELD] = new ValidationMetadata();
    }
    return prototype[PROTOTYPE_VALIDATION_FIELD];
};

export const add_field_validation = (prototype: any, fieldName: string, addedProperties: ValidationFieldMetadata) => {
    let meta = init_class_validation_prototype(prototype);
    if (!meta.fields[fieldName]) {
        meta.fields[fieldName] = new ValidationFieldMetadata();
        let t = Reflect.getMetadata("design:type", prototype, fieldName);
        if (!!t && t.name) {
            meta.fields[fieldName].dataType = t.name;
        }
    }
    meta.fields[fieldName] = Object.assign(meta.fields[fieldName], addedProperties);
};

export const Validate = () => {
    return <T extends { new(...args: any[]): {} }>(constructor: T) => {
        let meta = init_class_validation_prototype(constructor.prototype);
        meta.needValidate = true;
        return constructor;
    };
};

export const Required = () => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {required: true});
    };
};

export const Optional = () => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {required: false});
    };
};

export const DefaultValue = (value: string | number | boolean) => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {defaultValue: value});
    };
};

export const Range = (from: number, to: number) => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {inRange: {from, to}});
    };
};

export const InSet = (set: any[]) => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {inSet: set});
    };
};

export const SubSet = (set: any[], allowEmpty: boolean = true) => {
    return (prototype: any, propertyKey: string) => {
        IsArray(allowEmpty)(prototype, propertyKey);
        add_field_validation(prototype, propertyKey, {subSet: set});
    };
};

export const Match = (regex: string) => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {match: regex});
    };
};

export const IsInteger = () => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {isInteger: true});
    };
};

export const IsArray = (allowEmpty: boolean = true) => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {dataType: "Array", isArray: true, allowEmpty});
    };
};

export const GreaterThan = (value: number) => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {greaterThan: value});
    };
};

export const LessThan = (value: number) => {
    return (prototype: any, propertyKey: string) => {
        add_field_validation(prototype, propertyKey, {lessThan: value});
    };
};
