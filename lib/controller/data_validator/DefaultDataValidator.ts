import {AbstractDataValidator} from "./AbstractDataValidator";
import {ValidationReport} from "./ValidationReport";
import {Errors} from "../../system/Errors";
import {ValidationMetadata} from "./ValidationRules";

export class DefaultDataValidator extends AbstractDataValidator {

    public constructor() {
        super();
    }

    public async validate(meta: ValidationMetadata, data: any, strictTypeCheck: boolean): Promise<true | ValidationReport[]> {

        if (!meta || !meta.needValidate) {
            return true;
        }

        let validationReport: ValidationReport[] = [];
        let dataKeys = Object.keys(data || {});
        let validationKeys = Object.keys(meta.fields);

        dataKeys.forEach((key) => {
            if (validationKeys.indexOf(key) >= 0) {
                //-- validate this data
                let value = data[key];
                let fieldValidators = meta.fields[key];

                //-- parse back data type, only check if value existed or field is required
                if ((value === undefined) && (fieldValidators.defaultValue !== undefined)) {
                    value = fieldValidators.defaultValue;
                }

                if (fieldValidators.required || (value !== null && value !== undefined)) {
                    switch (fieldValidators.dataType) {
                        case "Array": {
                            if (!Array.isArray(value)) {
                                if (strictTypeCheck) {
                                    validationReport.push({field: key, message: Errors.validation.DATA_TYPE_ERROR});
                                    break;
                                } else if (typeof value === "string") {
                                    //-- try parse
                                    try {
                                        value = JSON.parse(value);
                                        if (!Array.isArray(value)) {
                                            validationReport.push({
                                                field: key,
                                                message: Errors.validation.DATA_TYPE_ERROR
                                            });
                                            break;
                                        }
                                    } catch (err) {
                                        //-- error parsing
                                        validationReport.push({field: key, message: Errors.validation.DATA_TYPE_ERROR});
                                        break;
                                    }
                                }
                            }
                            if (!fieldValidators.allowEmpty && !value.length) {
                                validationReport.push({field: key, message: Errors.validation.EMPTINESS_NOT_ALLOWED});
                                break;
                            }
                            data[key] = value;
                            break;
                        }
                        case "String":
                            //-- small "s"
                            if (strictTypeCheck && typeof value !== "string") {
                                validationReport.push({field: key, message: Errors.validation.DATA_TYPE_ERROR});
                                break;
                            }
                            data[key] = String(value);
                            break;

                        case "Boolean":
                            // @ts-ignore
                            if (!(strictTypeCheck && [true, false].includes(value)) && !(!strictTypeCheck && [true, false, "true", "false"].includes(value))) {
                                validationReport.push({field: key, message: Errors.validation.DATA_TYPE_ERROR});
                                break;
                            }
                            data[key] = value === true || value === "true";
                            break;
                        case "Number":
                            // @ts-ignore
                            if (isNaN(value) || (strictTypeCheck && typeof value !== "number")) {
                                validationReport.push({field: key, message: Errors.validation.DATA_TYPE_ERROR});
                                break;
                            }
                            data[key] = Number(value);
                            break;
                        default:
                            //-- do nothing
                            break;
                    }
                }

                //-- if parsing success then next
                if (!validationReport.length) {

                    if (fieldValidators.required && (value === undefined || value === null)) {
                        validationReport.push({field: key, message: Errors.validation.FIELD_REQUIRED});
                    }
                    if (fieldValidators.match) {
                        let regex = new RegExp(fieldValidators.match);
                        if (!regex.exec(value)) {
                            validationReport.push({field: key, message: Errors.validation.PATTERN_NOT_MATCHED});
                        }
                    }
                    if (fieldValidators.isInteger && parseInt(value) !== Number(value)) {
                        validationReport.push({field: key, message: Errors.validation.MUST_BE_INTEGER});
                    }
                    if (fieldValidators.lessThan !== undefined && value >= fieldValidators.lessThan) {
                        validationReport.push({field: key, message: Errors.validation.VALUE_NOT_IN_RANGE});
                    }
                    if (fieldValidators.greaterThan !== undefined && value <= fieldValidators.greaterThan) {
                        validationReport.push({field: key, message: Errors.validation.VALUE_NOT_IN_RANGE});
                    }
                    if (fieldValidators.inRange !== undefined && ((value < fieldValidators.inRange.from) || (value > fieldValidators.inRange.to))) {
                        validationReport.push({field: key, message: Errors.validation.VALUE_NOT_IN_RANGE});
                    }
                    if (fieldValidators.inSet !== undefined && (!fieldValidators.inSet.indexOf || fieldValidators.inSet.indexOf(value) < 0)) {
                        validationReport.push({field: key, message: Errors.validation.VALUE_OUT_OF_SET});
                    }
                    if (fieldValidators.subSet !== undefined && ((value as any[]).some((v) => fieldValidators.subSet!.indexOf(v) < 0))) {
                        validationReport.push({field: key, message: Errors.validation.VALUE_IS_NOT_SUBSET});
                    }
                }
            } else {
                //-- this field is not allowed
                validationReport.push({field: key, message: Errors.validation.FIELD_NOT_ALLOWED});
            }
        });

        //--
        validationKeys.forEach((key) => {
            if (dataKeys.indexOf(key) < 0 && meta.fields[key].required) {
                validationReport.push({field: key, message: Errors.validation.FIELD_REQUIRED});
            }
        });
        return validationReport.length ? validationReport : true;
    }

}
