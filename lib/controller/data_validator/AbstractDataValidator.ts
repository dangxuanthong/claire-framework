import {IDataValidator} from "./IDataValidator";
import {ValidationReport} from "./ValidationReport";
import {ValidationMetadata} from "./ValidationRules";
import {Initable} from "../../system/Initable";
import {IAppContext} from "../..";

export abstract class AbstractDataValidator implements IDataValidator, Initable {

    protected constructor() {
    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract validate(meta: ValidationMetadata, data: any, strictTypeCheck: boolean): Promise<true | ValidationReport[]>;

}
