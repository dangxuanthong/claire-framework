import {ValidationReport} from "./ValidationReport";
import {ValidationMetadata} from "./ValidationRules";

export interface IDataValidator {

    validate(meta: ValidationMetadata, data: any, strictTypeCheck: boolean): Promise<true | ValidationReport[]>;

}
