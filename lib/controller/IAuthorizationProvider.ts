import {Initable} from "../system/Initable";

export interface IAuthorizationProvider<T, U> extends Initable {

    /**
     * Must throw corresponding error if not authorized.
     * @param request
     * @param controllerMetadata
     */
    authorize(request: T, controllerMetadata: U): Promise<void>;

}
