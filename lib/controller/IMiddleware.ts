import {Initable} from "../system/Initable";

export interface IMiddleware<T> extends Initable {

    intercept(): (connection: T, nextFn: (err?: any) => void) => void;

}
