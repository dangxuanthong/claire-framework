import {AbstractHttpResponder, AbstractHttpMiddleware, AbstractAccessCondition} from "../..";
import {PROTOTYPE_VALIDATION_FIELD, ValidationMetadata} from "../data_validator/ValidationRules";
import {HTTP} from "./AbstractHttpController";
import {PermissionType} from "./RBAC";

export const CONTROLLER_META_DATA_KEY = "_controller_prototype";

export interface ControllerMetadata {
    permissionGroup?: string;
    handlers: ControllerHandlerMetadata[];
}

export interface ControllerHandlerMetadata {
    //-- http
    handler: string,
    override?: boolean,
    method?: HTTP,
    url?: string,
    responder?: AbstractHttpResponder,
    middleware?: AbstractHttpMiddleware[],
    //-- validator
    bodyValidator?: ValidationMetadata,
    paramsValidator?: ValidationMetadata;
    queryValidator?: ValidationMetadata;
    responseValidator?: ValidationMetadata,
    //-- permission related
    permissionGroup?: string,
    permissionName?: string,
    permissionType?: PermissionType,
    permissionConditions?: AbstractAccessCondition<any>[],
    openAccess?: boolean,
}


const initPrototype = (prototype: any): ControllerMetadata => {
    let property: any =  Object.getOwnPropertyDescriptor(prototype, CONTROLLER_META_DATA_KEY);
    let controllerMetadata: ControllerMetadata = property? property.value : undefined;
    if (!controllerMetadata) {
        controllerMetadata = {
            permissionGroup: undefined,
            handlers: [],
        };
        //-- set the prototype controller metadata
        prototype[CONTROLLER_META_DATA_KEY] = controllerMetadata;

        //-- add parent metadata
        let parentControllerMetadata: ControllerMetadata = Object.getPrototypeOf(prototype)[CONTROLLER_META_DATA_KEY];
        if (parentControllerMetadata) {
            controllerMetadata.permissionGroup = controllerMetadata.permissionGroup || parentControllerMetadata.permissionGroup;
            //-- merge handler metadata
            if (parentControllerMetadata.handlers) {
                parentControllerMetadata.handlers.forEach((handler) => {
                    //-- check if there is handler in superHandlers
                    let matchedIndex = controllerMetadata.handlers.findIndex((h) => (h.method === handler.method && h.url === handler.url) || (h.handler === handler.handler));
                    if (matchedIndex >= 0) {
                        controllerMetadata.handlers[matchedIndex] = {...handler, ...controllerMetadata.handlers[matchedIndex]};
                    } else {
                        controllerMetadata.handlers.push(handler);
                    }
                });
            }
        }
    }
    return controllerMetadata;
};

const initKey = (prototype: any, propertyKey: string): ControllerHandlerMetadata => {
    let controllerMetadata = initPrototype(prototype);
    let handlerMetadata = controllerMetadata.handlers.find((h) => h.handler === propertyKey);
    if (!handlerMetadata) {
        handlerMetadata = {
            handler: propertyKey
        };
        controllerMetadata.handlers.push(handlerMetadata);
    }
    return handlerMetadata;
};

export const Mapping = (method: HTTP, url: string, middleware?: AbstractHttpMiddleware[], responder?: AbstractHttpResponder) => {
    return (prototype: any, propertyKey: string) => {
        let handler = initKey(prototype, propertyKey);
        handler.method = method;
        handler.url = url;
        if (middleware !== undefined) {
            handler.middleware = middleware;
        }
        if (responder !== undefined) {
            handler.responder = responder
        }
    };
};

export const Validator = (bodyValidator?: any, paramsValidator?: any, queryValidator?: any, responseValidator?: any,) => {

    return (prototype: any, propertyKey: string) => {
        let handler = initKey(prototype, propertyKey);
        //-- allow merging for multiple same decorators
        if (bodyValidator !== undefined) {
            handler.bodyValidator = bodyValidator.prototype && bodyValidator.prototype[PROTOTYPE_VALIDATION_FIELD];
        }
        if (responseValidator !== undefined) {
            handler.responseValidator = responseValidator.prototype && responseValidator.prototype[PROTOTYPE_VALIDATION_FIELD];
        }
        if (paramsValidator !== undefined) {
            handler.paramsValidator = paramsValidator.prototype && paramsValidator.prototype[PROTOTYPE_VALIDATION_FIELD];
        }
        if (queryValidator !== undefined) {
            handler.queryValidator = queryValidator.prototype && queryValidator.prototype[PROTOTYPE_VALIDATION_FIELD];
        }
    };
};

export const PermissionGroup = (groupName: string) => {

    return <T extends { new(...args: any[]): {} }>(constructor: T) => {
        let controllerMetadata = initPrototype(constructor.prototype);
        controllerMetadata.permissionGroup = groupName;
        return constructor;
    };
};

export const Permission = (permissionConditions?: any[]) => {
    return (prototype: any, propertyKey: string) => {
        let handler = initKey(prototype, propertyKey);
        if (permissionConditions !== undefined) {
            handler.permissionConditions = permissionConditions;
        }
    }
};

export const PermissionName = (permissionName: string) => {
    return (prototype: any, propertyKey: string) => {
        let handler = initKey(prototype, propertyKey);
        handler.permissionName = permissionName;
    }
};

export const Override = () => {
    return (prototype: any, propertyKey: string) => {
        let handler = initKey(prototype, propertyKey);
        handler.override = true;
    }
};

export const OpenAccess = () => {

    return (prototype: any, propertyKey: string) => {
        let handler = initKey(prototype, propertyKey);
        handler.openAccess = true;
    }
};
