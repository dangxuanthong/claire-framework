import bodyParser from "body-parser";
import fileUpload from "express-fileupload";
import {AbstractHttpMiddleware} from "./AbstractHttpMiddleware";
import {HttpConnection} from "./HttpConnection";

export class BodyParser extends AbstractHttpMiddleware {

    private readonly maxFileNumber: number;
    private readonly maxFileSize: number;
    private readonly mapFileToBody: boolean;


    public constructor(maxFileNumber: number, maxFileSizeInByte: number, mapFileToBody: boolean = true) {
        super();
        this.maxFileNumber = maxFileNumber;
        this.maxFileSize = maxFileSizeInByte;
        this.mapFileToBody = mapFileToBody;
    }

    public intercept() {
        return (connection: HttpConnection, next: (err?: any) => void): void => {
            //-- parse json
            return bodyParser.json()(connection.request, connection.response, () => {
                //- parse uploaded files
                fileUpload({
                    limits: {
                        files: this.maxFileNumber,
                        fileSize: this.maxFileSize,
                    }
                })(connection.request, connection.response, () => {
                    //-- map file to body
                    if (this.mapFileToBody) {
                        let files = connection.request.files;
                        if (files) {
                            let keys = Object.keys(files);
                            for (let i = 0; i < keys.length; i++) {
                                connection.request.body[keys[i]] = files[keys[i]];
                            }
                        }
                    }
                    next();
                });
            });
        }
    }

}
