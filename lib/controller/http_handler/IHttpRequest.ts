import {Request} from "express";

export interface IHttpRequest extends Request {

    getQuery<T extends any>(classInstance?: new(...args: any[]) => T): T;

    getParams<T extends any>(classInstance?: new(...args: any[]) => T): T;

    getBody<T extends any>(classInstance?: new(...args: any[]) => T): T;

    getValue<T extends any>(key: string): T;

    setValue<T extends any>(key: string, value: T): void;

}
