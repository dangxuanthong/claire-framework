import {Server} from "http";
import express, {NextFunction, Request, Response} from "express";
import {
    AbstractHttpMiddleware,
    IHttpRequest,
    IHttpResponse,
    ClaireError,
    DefaultJsonResponder,
    DefaultDataValidator,
    DefaultHttpErrorHandler,
    AbstractHttpController,
    AbstractHttpAuthorizationProvider, HTTP,
} from "../..";
import {Errors} from "../../system/Errors";
import {AbstractHttpRequestHandler} from "./AbstractHttpRequestHandler";

export class DefaultHttpRequestHandler extends AbstractHttpRequestHandler {

    public constructor(
        listenPort: number,
        mountUrl: string,
        middleware: (AbstractHttpMiddleware | undefined)[],
        controllers: (AbstractHttpController | undefined)[],
        authorizationProvider?: AbstractHttpAuthorizationProvider,
    ) {
        super(listenPort, new DefaultDataValidator(), new DefaultHttpErrorHandler(), mountUrl, middleware, controllers, authorizationProvider);
    }

    public configure(server: Server): Server {
        const app = express();

        //-- use global middleware
        const requestUpgradeMiddleware = (req: Request, res: Response, next: NextFunction): any => {
            // @ts-ignore
            req["valueMap"] = new Map<string, any>();
            // @ts-ignore
            req["getQuery"] = () => req.query;
            // @ts-ignore
            req["getBody"] = () => req.body;
            // @ts-ignore
            req["getParams"] = () => req.params;
            // @ts-ignore
            req["getValue"] = (key: string) => req["valueMap"].get(key);
            // @ts-ignore
            req["setValue"] = (key: string, value: any) => req["valueMap"].set(key, value);
            return next();
        };

        let globalMiddleware = this.middleware.map((m) => {
            return (request: Request, response: Response, next: NextFunction): any => {
                try {
                    return m.intercept()({
                        request: request as IHttpRequest,
                        response: response as IHttpResponse
                    }, next);
                } catch (err) {
                    this.errorHandler.handle({
                        request: request as IHttpRequest,
                        response: response as IHttpResponse
                    }, err);
                }
            };
        });
        app.use([requestUpgradeMiddleware, ...globalMiddleware]);

        let defaultJsonResponder = new DefaultJsonResponder();

        //-- process routers
        const expressRouter = express.Router();

        for (let i = 0; i < this.controllers.length; i++) {
            let controller = this.controllers[i];
            const routes = controller.getHandlers();

            for (let j = 0; j < routes.length; j++) {
                let route = routes[j];
                let middleware = (route.middleware || []).map((m) => {
                    return (req: Request, res: Response, next: NextFunction): any => {
                        try {
                            return m.intercept()({
                                request: req as IHttpRequest,
                                response: res as IHttpResponse
                            }, next);
                        } catch (err) {
                            this.errorHandler.handle({
                                request: req as IHttpRequest,
                                response: res as IHttpResponse
                            }, err);
                        }
                    };
                });

                let expressHandler: any;
                switch (route.method) {
                    case HTTP.GET:
                        expressHandler = expressRouter.get.bind(expressRouter);
                        break;
                    case HTTP.POST:
                        expressHandler = expressRouter.post.bind(expressRouter);
                        break;
                    case HTTP.DELETE:
                        expressHandler = expressRouter.delete.bind(expressRouter);
                        break;
                    case HTTP.PUT:
                        expressHandler = expressRouter.put.bind(expressRouter);
                        break;
                    default:
                        expressHandler = undefined;
                }

                let _this = this;
                !!expressHandler && expressHandler(route.url, middleware, function (req: Request, res: Response) {
                    //-- validate
                    (async (_this: DefaultHttpRequestHandler) => {
                        if (route.queryValidator !== undefined) {
                            //-- disable strict type checking because query values are always string
                            let validationResult = await _this.dataValidator.validate(route.queryValidator, req.query, false);
                            if (validationResult !== true) {
                                //-- return validation report
                                throw new ClaireError(Errors.validation.VALIDATION_ERROR, validationResult);
                            }
                        }
                        if (route.paramsValidator !== undefined) {
                            //-- disable strict type checking because param values are always string
                            let validationResult = await _this.dataValidator.validate(route.paramsValidator, req.params, false);
                            if (validationResult !== true) {
                                //-- return validation report
                                throw new ClaireError(Errors.validation.VALIDATION_ERROR, validationResult);
                            }
                        }
                        if (route.bodyValidator !== undefined) {
                            //- enable strict type checking
                            let validationResult = await _this.dataValidator.validate(route.bodyValidator, req.body, true);
                            if (validationResult !== true) {
                                //-- return validation report
                                throw new ClaireError(Errors.validation.VALIDATION_ERROR, validationResult);
                            }
                        }

                        //-- call authorization provider
                        if (!route.openAccess && _this.authorizationProvider) {
                            await _this.authorizationProvider.authorize(req as IHttpRequest, route);
                        }

                        // @ts-ignore
                        let result = await controller[route.handler](req as IHttpRequest);
                        if (route.responseValidator !== undefined) {
                            //-- enable strict type checking for response
                            let validationResult = await _this.dataValidator.validate(route.responseValidator, result, true);
                            if (validationResult !== true) {
                                //-- this is the system error
                                throw new ClaireError(Errors.validation.BACKEND_RETURN_ERROR_DATA, validationResult);
                            }
                        }
                        return result;
                    })(_this)
                        .then((result: any) => {
                            let responder = route.responder || defaultJsonResponder;
                            responder.response(res, result);
                        })
                        .catch((err: any) => {
                            _this.errorHandler.handle({
                                request: req as IHttpRequest,
                                response: res as IHttpResponse
                            }, err);
                        });
                });
            }
        }

        app.use(this.mountUrl, expressRouter);

        //-- last resort for error handling
        app.use((req: Request, res: Response) => {
            this.errorHandler.handle({
                request: req as IHttpRequest,
                response: res as IHttpResponse
            }, new ClaireError(Errors.system.HANDLER_NOT_FOUND));
        });

        return require("http")["Server"](app);
    }

}
