import {AbstractRequestHandler} from "../AbstractRequestHandler";
import {
    Initable,
    AbstractHttpController,
    AbstractHttpMiddleware,
    HttpConnection,
    IAppContext,
    AbstractHttpAuthorizationProvider
} from "../..";
import {AbstractDataValidator} from "../data_validator/AbstractDataValidator";
import {AbstractHttpErrorHandler} from "../error_handler/AbstractHttpErrorHandler";

export abstract class AbstractHttpRequestHandler extends AbstractRequestHandler<HttpConnection> implements Initable {

    public readonly mountUrl: string;
    public middleware: AbstractHttpMiddleware[];
    public controllers: AbstractHttpController[];

    protected constructor(listenPort: number,
                          dataValidator: AbstractDataValidator,
                          errorHandler: AbstractHttpErrorHandler,
                          mountUrl: string, middleware: (AbstractHttpMiddleware | undefined)[],
                          controllers: (AbstractHttpController | undefined)[],
                          authorizationProvider?: AbstractHttpAuthorizationProvider) {
        super(listenPort, dataValidator, errorHandler, middleware, controllers, authorizationProvider);
        this.mountUrl = mountUrl;
    }

    public async init(appContext: IAppContext): Promise<void> {
        await super.init(appContext);
        //-- load and check http handlers
        appContext.getHttpEndpoints();
        return;
    }

    public async stop(): Promise<void> {
        await super.stop();
        return;
    }

}
