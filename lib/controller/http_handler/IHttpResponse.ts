import {Response} from "express";

export interface IHttpResponse extends Response {
}