import {
    AbstractHttpController,
    belongs,
    ClaireError,
    DataType,
    DefaultValue,
    eq,
    HTTP,
    IAppContext,
    IHttpRequest,
    IQuery,
    IQueryProvider,
    IsArray,
    PermissionType,
    regex,
    Required,
    SubSet,
    Validate, ControllerHandlerMetadata, AbstractModel,
} from "../..";
import {ModelMetaData} from "../../model/ModelMetaData";
import {MODEL_PROTOTYPE_METADATA_FIELD} from "../../model/TableMapper";
import {Errors} from "../../system/Errors";
import {add_field_validation, PROTOTYPE_VALIDATION_FIELD, ValidationMetadata} from "../data_validator/ValidationRules";

export class DefaultHttpResourceController<T extends AbstractModel> extends AbstractHttpController {

    protected readonly model: new (...args: any[]) => T;
    protected readonly modelMetadata: ModelMetaData;
    protected queryProvider: IQuery<T>;

    constructor(model: new(...args: any[]) => T) {
        super();
        this.model = model;
        this.modelMetadata = model.prototype[MODEL_PROTOTYPE_METADATA_FIELD];
        if (!this.modelMetadata) {
            throw new ClaireError(Errors.database.MODEL_METADATA_NOT_FOUND);
        }
        this.modelMetadata.assertValid();
    }

    public async init(appContext: IAppContext): Promise<void> {
        await super.init(appContext);
        this.queryProvider = appContext.getDatabaseAdapter().use(this.model);
    }

    public getHandlers(): ControllerHandlerMetadata[] {

        const getDataType = (type: DataType | undefined): string => {
            switch (type) {
                case DataType.BOOL:
                    return "Boolean";
                case DataType.INTEGER:
                case DataType.FLOAT:
                    return "Number";
                case DataType.TEXT:
                case DataType.STRING:
                    return "String";
                default:
                    return "";
            }
        };

        const createResource = {
            request: {
                prototype: {
                    [PROTOTYPE_VALIDATION_FIELD]: new ValidationMetadata()
                }
            },
            response: {
                prototype: {
                    [PROTOTYPE_VALIDATION_FIELD]: new ValidationMetadata()
                }
            },
        };

        const getResource = {
            params: {
                prototype: {
                    [PROTOTYPE_VALIDATION_FIELD]: new ValidationMetadata()
                }
            },
            response: {
                prototype: {
                    [PROTOTYPE_VALIDATION_FIELD]: new ValidationMetadata()
                }
            },
            query: {
                prototype: {
                    [PROTOTYPE_VALIDATION_FIELD]: new ValidationMetadata()
                }
            },
        };

        const updateResource = {
            request: {
                prototype: {
                    [PROTOTYPE_VALIDATION_FIELD]: new ValidationMetadata()
                }
            },
        };

        const getAllResources = {
            query: {
                prototype: {
                    [PROTOTYPE_VALIDATION_FIELD]: new ValidationMetadata()
                }
            },
        };

        //-- prototyping createResource request
        {
            // @ts-ignore
            Validate()(createResource.request);
            this.modelMetadata.getFields().filter(f => !f.isAutoGen).forEach((field) => {
                if (!field.isPrimaryKey || !field.isAutoIncrement) {
                    let dataType = getDataType(field.dataType);
                    add_field_validation(createResource.request.prototype, field.fieldLogicName!, {dataType});
                    if (!field.nullable) {
                        Required()(createResource.request.prototype, field.fieldLogicName!);
                    }
                    if (field.defaultValue !== undefined) {
                        add_field_validation(createResource.request.prototype, field.fieldLogicName!, {required: false});
                    }
                    if (field.defaultValue !== undefined) {
                        DefaultValue(field.defaultValue)(createResource.request.prototype, field.fieldLogicName!);
                    }
                }
            });
        }

        //-- prototyping createResource response
        {
            // @ts-ignore
            Validate()(createResource.response);
            let field = this.modelMetadata.getPrimaryKey();
            add_field_validation(createResource.response.prototype, field.fieldLogicName!, {
                dataType: getDataType(field.dataType),
                required: true,
            });
        }

        //-- prototyping getResource params
        {
            // @ts-ignore
            Validate()(getResource.params);
            let field = this.modelMetadata.getPrimaryKey();
            add_field_validation(getResource.params.prototype, field.fieldLogicName!, {
                dataType: getDataType(field.dataType),
                required: true,
            });
        }

        //-- prototyping getResource query
        {
            // @ts-ignore
            Validate()(getResource.query);
            SubSet(this.modelMetadata.getFields().map(f => f.fieldLogicName!), false)(getResource.query.prototype, "filter");
        }

        //-- prototyping getAllResources query
        {
            // @ts-ignore
            Validate()(getAllResources.query);
            SubSet(this.modelMetadata.getFields().map(f => f.fieldLogicName!), false)(getAllResources.query.prototype, "filter");

            this.modelMetadata.getFields().forEach((f) => {
                if (f.isPrimaryKey || f.isForeignKey) {
                    //-- for primary key, getAllResource accepts and array of ids
                    add_field_validation(getAllResources.query.prototype, f.fieldLogicName!, {required: false});
                    IsArray(false)(getAllResources.query.prototype, f.fieldLogicName!);
                } else {
                    add_field_validation(getAllResources.query.prototype, f.fieldLogicName!, {
                        required: false,
                        dataType: getDataType(f.dataType),
                    });
                }
            });
            add_field_validation(getAllResources.query.prototype, "limit", {
                dataType: "Number",
                required: false,
                isInteger: true,
                greaterThan: 0,
            });
            add_field_validation(getAllResources.query.prototype, "page", {
                dataType: "Number",
                required: false,
                isInteger: true,
                greaterThan: 0,
            });
        }

        //-- prototyping getResource response
        {
            // @ts-ignore
            Validate()(getResource.response);
            this.modelMetadata.getFields().forEach((field) => {
                add_field_validation(getResource.response.prototype, field.fieldLogicName!, {dataType: getDataType(field.dataType)});
                if (field.nullable) {
                    add_field_validation(getResource.response.prototype, field.fieldLogicName!, {required: false});
                }
            });
        }

        //-- prototyping updateResource request
        // @ts-ignore
        Validate()(updateResource.request);
        this.modelMetadata.getFields().filter(f => !f.isAutoGen).forEach((field) => {
            //skip primary key
            if (!field.isPrimaryKey) {
                add_field_validation(updateResource.request.prototype, field.fieldLogicName!, {dataType: getDataType(field.dataType)});
            }
        });

        let permissionGroup = `${this.modelMetadata.modelName}Management`;
        let handlers: ControllerHandlerMetadata[] = [
            {
                method: HTTP.POST,
                url: `/${this.modelMetadata.modelName!.toLowerCase()}`,
                handler: "createResource",
                responder: undefined, // use default JSON responder
                middleware: undefined,
                paramsValidator: undefined,
                bodyValidator: createResource.request.prototype[PROTOTYPE_VALIDATION_FIELD],
                responseValidator: createResource.response.prototype[PROTOTYPE_VALIDATION_FIELD],
                override: false,
                permissionName: `create${this.model.name}`,
                permissionGroup,
                permissionType: PermissionType.WRITE,
            },
            {
                method: HTTP.GET,
                url: `/${this.modelMetadata.modelName!.toLowerCase()}/:${this.modelMetadata.getPrimaryKey().fieldLogicName}`,
                handler: "getResource",
                responder: undefined, // use default JSON responder
                middleware: undefined,
                paramsValidator: getResource.params.prototype[PROTOTYPE_VALIDATION_FIELD],
                bodyValidator: undefined,
                responseValidator: getResource.response.prototype[PROTOTYPE_VALIDATION_FIELD],
                queryValidator: getResource.query.prototype[PROTOTYPE_VALIDATION_FIELD],
                override: false,
                permissionName: `get${this.model.name}`,
                permissionGroup,
                permissionType: PermissionType.READ,
            },
            {
                method: HTTP.GET,
                url: `/${this.modelMetadata.modelName!.toLowerCase()}`,
                handler: "getAllResources",
                responder: undefined, // use default JSON responder
                middleware: undefined,
                paramsValidator: undefined,
                bodyValidator: undefined,
                responseValidator: undefined,
                queryValidator: getAllResources.query.prototype[PROTOTYPE_VALIDATION_FIELD],
                override: false,
                permissionName: `getAll${this.model.name}`,
                permissionGroup,
                permissionType: PermissionType.READ,
            },
            {
                method: HTTP.PUT,
                url: `/${this.modelMetadata.modelName!.toLowerCase()}/:${this.modelMetadata.getPrimaryKey().fieldLogicName}`,
                handler: "updateResource",
                responder: undefined, // use default JSON responder
                middleware: undefined,
                paramsValidator: getResource.params.prototype[PROTOTYPE_VALIDATION_FIELD], //-- share the same params validator
                bodyValidator: updateResource.request.prototype[PROTOTYPE_VALIDATION_FIELD],
                responseValidator: undefined,
                override: false,
                permissionName: `update${this.model.name}`,
                permissionGroup,
                permissionType: PermissionType.WRITE,
            },
            {
                method: HTTP.DELETE,
                url: `/${this.modelMetadata.modelName!.toLowerCase()}/:${this.modelMetadata.getPrimaryKey().fieldLogicName}`,
                handler: "deleteResource",
                responder: undefined, // use default JSON responder
                middleware: undefined,
                paramsValidator: getResource.params.prototype[PROTOTYPE_VALIDATION_FIELD], //-- share the same params validator
                bodyValidator: undefined,
                responseValidator: undefined,
                permissionName: `delete${this.model.name}`,
                override: false,
                permissionGroup,
                permissionType: PermissionType.WRITE,
            },
        ];
        return AbstractHttpController.mergeHandlerMetadata(handlers, super.getHandlers());
    }

    public async createResource(request: IHttpRequest, transaction?: IQueryProvider): Promise<T> {
        let provider = transaction ? transaction.use(this.model) : this.queryProvider;
        let body: any = request.getBody();

        let instance = new this.model();
        //-- append all properties
        this.modelMetadata.getFields().forEach((field) => {
            // @ts-ignore
            instance[field.fieldLogicName!] = body[field.fieldLogicName!];
        });

        instance = await provider.saveOne((instance));
        return {
            // @ts-ignore
            [this.modelMetadata.getPrimaryKey().fieldLogicName!]: instance[this.modelMetadata.getPrimaryKey().fieldLogicName!]
        } as T;
    }

    public async getResource(request: IHttpRequest, transaction?: IQueryProvider): Promise<T> {
        let provider = transaction ? transaction.use(this.model) : this.queryProvider;
        let primaryKey = this.modelMetadata.getPrimaryKey().fieldLogicName!;
        // @ts-ignore
        let resource = await provider.getOne([{[primaryKey]: eq(request.getParams()[primaryKey])}], {projection: request.getQuery()["filter"]});
        if (!resource) {
            throw new ClaireError(Errors.database.RECORD_NOT_FOUND);
        }
        return resource as T;
    }

    public async getAllResources(request: IHttpRequest, transaction?: IQueryProvider) {
        let provider = transaction ? transaction.use(this.model) : this.queryProvider;
        let queryConditions: any[] = [];
        this.modelMetadata.getFields().forEach((f) => {
            let value = request.getQuery()[f.fieldLogicName!];
            if (value !== undefined) {
                if ([DataType.STRING, DataType.TEXT].includes(f.dataType!)) {
                    //-- if string then find by regex
                    queryConditions.push({[f.fieldLogicName!]: regex(value)});
                } else if (f.isPrimaryKey || f.isForeignKey) {
                    //-- query is array for primary and foreign key
                    queryConditions.push({[f.fieldLogicName!]: belongs(value)});
                } else {
                    //-- find by equal for the rest
                    queryConditions.push({[f.fieldLogicName!]: eq(value)});
                }
            }
        });

        return await provider.getMany(queryConditions, {
            projection: request.getQuery()["filter"],
            limit: request.getQuery()["limit"],
            page: request.getQuery()["page"]
        });
    }

    public async updateResource(request: IHttpRequest, transaction?: IQueryProvider): Promise<void> {
        let provider = transaction ? transaction.use(this.model) : this.queryProvider;
        let primaryKey = this.modelMetadata.getPrimaryKey().fieldLogicName!;
        // @ts-ignore
        let updateNumber = await provider.updateMany([{[primaryKey]: eq(request.getParams()[primaryKey])}], request.getBody());
        if (!updateNumber) {
            throw new ClaireError(Errors.database.RECORD_NOT_FOUND);
        }
        return;
    }

    public async deleteResource(request: IHttpRequest, transaction?: IQueryProvider): Promise<void> {
        let provider = transaction ? transaction.use(this.model) : this.queryProvider;
        let primaryKey = this.modelMetadata.getPrimaryKey().fieldLogicName!;
        // @ts-ignore
        let instance = await provider.deleteMany([{[primaryKey]: eq(request.getParams()[primaryKey])}]);
        if (instance === 0) {
            throw new ClaireError(Errors.database.RECORD_NOT_FOUND);
        }
        return;
    }

}
