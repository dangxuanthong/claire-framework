import {IAppContext, IHttpRequest} from "../../";

export enum ConditionValueType {

    /**
     * Any string value. Constraint is a regex.
     */
    STRING,

    /**
     * Numerical value. Constraint is {from: number, to: number, integer: boolean} encoded into string.
     */
    NUMBER,

    /**
     * Yes or No. No constraint.
     */
    BOOLEAN,

    /**
     * An unlimited list of string values. Constraint is whether emptiness is allowed.
     */
    LIST,

    /**
     * A list of choices. Constraint is the list of choices.
     */
    CHOICES,

    /**
     * A list of mutual exclusive choices. Constraint is the list of choices.
     */
    MUTEXCHOICES,
}

export interface ConditionPrototype {
    /**
     * Distinguishable name of this condition type.
     */
    operatorName: string;

    /**
     * Type of condition value. This is to guide the frontend to better display the input.
     */
    valueType: ConditionValueType;

    /**
     * Constraint for the condition value, encoded to string. This is to guide the frontend to better display the input.
     */
    valueConstraint?: string;
}

export interface IAccessCondition<T extends any> {
    getConditionPrototype(): ConditionPrototype;

    validate(requestedValue: T, permittedValue: T): Promise<boolean>;
}

export abstract class AbstractAccessCondition<T extends any> implements IAccessCondition<T> {

    /**
     * This resolver will resolve to the request value to be supplied to the validate function.
     */
    public requestedValueResolver: (request: IHttpRequest, appContext: IAppContext) => Promise<T>;

    protected constructor(requestedValueResolver: (request: IHttpRequest, appContext: IAppContext) => Promise<T>) {
        this.requestedValueResolver = requestedValueResolver;
    }

    public abstract getConditionPrototype(): ConditionPrototype;

    public abstract validate(requestedValue: T, permittedValue: T): Promise<boolean>;

}

export enum PermissionType {
    READ = "read",
    WRITE = "write",
}
