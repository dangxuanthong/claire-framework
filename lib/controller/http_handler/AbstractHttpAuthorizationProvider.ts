import {ControllerHandlerMetadata, IAppContext} from "../..";
import {IHttpRequest} from "./IHttpRequest";
import {IAuthorizationProvider} from "../IAuthorizationProvider";

export abstract class AbstractHttpAuthorizationProvider implements IAuthorizationProvider<IHttpRequest, ControllerHandlerMetadata> {

    protected constructor() {

    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract authorize(request: IHttpRequest, controllerMetadata: ControllerHandlerMetadata): Promise<void>;

}
