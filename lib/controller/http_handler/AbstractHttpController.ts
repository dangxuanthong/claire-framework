import {IRequestController} from "../IRequestController";
import {IAppContext, ILogger} from "../..";
import {CONTROLLER_META_DATA_KEY, ControllerHandlerMetadata, ControllerMetadata} from "./ControllerMetadata";
import {PermissionType} from "./RBAC";

export enum HTTP {
    GET = "get",
    POST = "post",
    PUT = "put",
    DELETE = "delete",
}

export abstract class AbstractHttpController implements IRequestController<ControllerHandlerMetadata> {

    private logger: ILogger;
    private handlers: ControllerHandlerMetadata[];

    protected constructor() {
    }

    public async init(appContext: IAppContext): Promise<void> {
        this.logger = appContext.getLogger();
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    private static mergeTwoHandlers(source: ControllerHandlerMetadata, overrider: ControllerHandlerMetadata) {
        let handler = {...source, ...overrider};
        handler.permissionType = handler.method === HTTP.GET ? PermissionType.READ : PermissionType.WRITE;
        return handler;
    }

    public getHandlers(): ControllerHandlerMetadata[] {
        //-- extract route from metadata
        if (!this.handlers) {
            let controllerMetadata: ControllerMetadata = Object.getPrototypeOf(this)[CONTROLLER_META_DATA_KEY];

            if (!controllerMetadata) {
                this.handlers = [];
            } else {
                this.handlers = controllerMetadata.handlers.map((handler) => ({
                    ...handler,
                    permissionName: handler.permissionName || handler.handler,
                    permissionGroup: handler.permissionGroup || controllerMetadata.permissionGroup,
                    permissionType: handler.method ? (handler.method === HTTP.GET ? PermissionType.READ : PermissionType.WRITE) : undefined,
                }));
            }
        }
        return this.handlers;
    }

    protected static mergeHandlerMetadata(sourceHandlers: ControllerHandlerMetadata[], overrideHandlers: ControllerHandlerMetadata[]): ControllerHandlerMetadata[] {
        let result: ControllerHandlerMetadata[] = sourceHandlers.slice();
        overrideHandlers.forEach(handler => {
            let matchedIndex = result.findIndex(h => h.method === handler.method && h.url === handler.url || (h.handler === handler.handler));
            if (matchedIndex >= 0) {
                result[matchedIndex] = AbstractHttpController.mergeTwoHandlers(result[matchedIndex], handler);
            } else {
                result.push(handler);
            }
        });
        return result;
    }
}
