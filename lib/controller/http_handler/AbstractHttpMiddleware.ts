import {IMiddleware} from "../IMiddleware";
import {HttpConnection} from "./HttpConnection";
import {IAppContext} from "../..";

export abstract class AbstractHttpMiddleware implements IMiddleware<HttpConnection> {

    protected constructor() {

    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract intercept(): (connection: HttpConnection, nextFn: (err?: any) => void) => void;

}
