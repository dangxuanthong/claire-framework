import {IHttpResponse} from "./IHttpResponse";
import {IHttpRequest} from "./IHttpRequest";

export interface HttpConnection {
    request: IHttpRequest,
    response: IHttpResponse
}
