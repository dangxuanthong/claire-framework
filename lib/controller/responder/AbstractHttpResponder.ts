import {IResponder} from "./IResponder";
import {IHttpResponse} from "../..";

export abstract class AbstractHttpResponder implements IResponder<IHttpResponse> {

    protected constructor() {
    }

    public abstract response(responseObject: IHttpResponse, value: any): void;

}


