import stream from 'stream';
import {AbstractHttpResponder} from "./AbstractHttpResponder";
import {IHttpResponse} from "../..";

export class DefaultFileResponder extends AbstractHttpResponder {

    public constructor() {
        super();
    }

    public response(response: IHttpResponse, value: { buffer: Buffer, fileName: string, contentType?: string }): void {
        const readStream = new stream.PassThrough();
        readStream.end(value.buffer);
        response.set('Content-disposition', 'attachment; filename=' + value.fileName);
        response.set('Content-Type', value.contentType || 'text/plain');
        readStream.pipe(response);
    }

}
