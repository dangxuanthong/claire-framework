export interface IResponder<T> {

    response(responseObject: T, value: any): void;

}
