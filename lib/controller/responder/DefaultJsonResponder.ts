import {IHttpResponse} from "../..";
import {AbstractHttpResponder} from "./AbstractHttpResponder";

export class DefaultJsonResponder extends AbstractHttpResponder {

    public constructor() {
        super();
    }

    public response(response: IHttpResponse, value: any): void {
        response.json({success: true, result: value});
    }

}
