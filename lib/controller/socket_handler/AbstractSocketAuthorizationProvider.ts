import {IAppContext} from "../../system/IAppContext";
import {SocketConnection} from "../socket_handler/SocketConnection";
import {IAuthorizationProvider} from "../IAuthorizationProvider";
import {SocketChannelHandler} from "../socket_handler/SocketChannelHandler";

export abstract class AbstractSocketAuthorizationProvider implements IAuthorizationProvider<SocketConnection, SocketChannelHandler> {

    protected constructor() {

    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract authorize(request: SocketConnection, controllerMetadata: SocketChannelHandler): Promise<void>;

}
