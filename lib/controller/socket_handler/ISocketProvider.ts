import {ISocket} from "./ISocket";

export interface ISocketProvider {

    all(): ISocket[];

    byId(id: string): ISocket | undefined;

}
