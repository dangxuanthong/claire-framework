import {IMiddleware} from "../IMiddleware";
import {SocketConnection} from "./SocketConnection";
import {IAppContext} from "../..";

export abstract class AbstractSocketMiddleware implements IMiddleware<SocketConnection> {

    protected constructor() {

    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract intercept(): (connection: SocketConnection, nextFn: (err?: any) => void) => void;

}
