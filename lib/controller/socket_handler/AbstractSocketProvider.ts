import {ISocketProvider} from "./ISocketProvider";
import {ISocket} from "./ISocket";

export abstract class AbstractSocketProvider implements ISocketProvider {
    protected constructor() {

    }

    public abstract all(): ISocket[];

    public abstract byId(id: string): ISocket | undefined;

}
