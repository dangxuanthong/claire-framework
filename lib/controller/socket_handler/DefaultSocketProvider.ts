import {AbstractSocketProvider} from "./AbstractSocketProvider";
import {ISocket} from "./ISocket";
import {AbstractSocketRequestHandler} from "./AbstractSocketRequestHandler";

export class DefaultSocketProvider extends AbstractSocketProvider {

    private requestHandler: AbstractSocketRequestHandler;

    public constructor(requestHandler: AbstractSocketRequestHandler) {
        super();
        this.requestHandler = requestHandler;
    }

    all(): ISocket[] {
        return this.requestHandler.getConnectedSockets();
    }

    byId(id: string): ISocket | undefined {
        let sockets = this.all();
        return sockets.find((s) => s.id === id);
    }

}
