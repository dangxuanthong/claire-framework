import {PROTOTYPE_VALIDATION_FIELD, ValidationMetadata} from "../data_validator/ValidationRules";

export interface SocketChannelHandler {
    handler: string;
    channel: string;
    messageValidator?: ValidationMetadata,
    override: boolean;
}

export const CONTROLLER_META_DATA_FIELD = "_controller_prototype";

const setControllerPrototype = (prototype: any, propertyKey: string, channel: string, messageValidator?: any) => {
    if (!prototype[CONTROLLER_META_DATA_FIELD]) {
        prototype[CONTROLLER_META_DATA_FIELD] = {};
    }
    prototype[CONTROLLER_META_DATA_FIELD][propertyKey] = {
        handler: propertyKey,
        channel,
        messageValidator: messageValidator && messageValidator.prototype && messageValidator.prototype[PROTOTYPE_VALIDATION_FIELD],
    } as SocketChannelHandler;
};

export const Message = (channel: string, messageValidator?: any) => {
    return (prototype: any, propertyKey: string) => {
        setControllerPrototype(prototype, propertyKey, channel, messageValidator);
    };
};
