import {IRequestController} from "../IRequestController";
import {CONTROLLER_META_DATA_FIELD, SocketChannelHandler} from "./SocketChannelHandler";
import {IAppContext} from "../..";

export abstract class AbstractSocketController implements IRequestController<SocketChannelHandler> {

    protected constructor() {
    }

    public getHandlers(): SocketChannelHandler[] {
        //-- extract route from metadata
        let controllerMetadata = Object.getPrototypeOf(this)[CONTROLLER_META_DATA_FIELD];
        if (!controllerMetadata) {
            return [];
        }
        return Object.keys(controllerMetadata).map(key => {
            return controllerMetadata[key];
        });
    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

}
