import {Server} from "http";
import Io from "socket.io";
import {ClaireError, DefaultDataValidator} from "../..";
import {AbstractSocketMiddleware} from "./AbstractSocketMiddleware";
import {AbstractSocketController} from "./AbstractSocketController";
import {SocketChannelHandler} from "./SocketChannelHandler";
import {Errors} from "../../system/Errors";
import {ISocket} from "./ISocket";
import {AbstractSocketRequestHandler} from "./AbstractSocketRequestHandler";
import {DefaultSocketErrorHandler} from "../error_handler/DefaultSocketErrorHandler";
import {AbstractSocketAuthorizationProvider} from "./AbstractSocketAuthorizationProvider";

export class DefaultSocketRequestHandler extends AbstractSocketRequestHandler {

    private io: Io.Server;

    public constructor(
        listenPort: number,
        mountUrl: string,
        middleware: (AbstractSocketMiddleware | undefined)[],
        controllers: (AbstractSocketController | undefined)[],
        authorizationProvider: AbstractSocketAuthorizationProvider) {
        super(listenPort, new DefaultDataValidator(), new DefaultSocketErrorHandler(), middleware, mountUrl, controllers, authorizationProvider);

    }

    public configure(server: Server): Server {
        this.io = Io(server, {
            path: this.mountUrl
        });

        let socketMiddleware = this.middleware.map((m) => {
            return (socket: ISocket, err: any) => {
                try {
                    return m.intercept()({socket}, err);
                } catch (err) {
                    this.errorHandler.handle({socket}, err);
                }
            }
        });
        socketMiddleware.forEach((m) => this.io.use(m));

        this.controllers.forEach((controller) => {
            let routes: SocketChannelHandler[] = controller.getHandlers();
            this.io.on("connection", (socket: ISocket) => {
                this.onSocketConnect(socket);
                routes.forEach((route) => {
                    socket.on(route.channel, (data: any, callback: any) => {
                        (async () => {
                            if (route.messageValidator) {
                                let result = await this.dataValidator.validate(route.messageValidator, data, true);
                                if (result !== true) {
                                    throw new ClaireError(Errors.validation.VALIDATION_ERROR, result);
                                }
                            }
                            await (controller as any)[route.handler](socket, data);
                        })()
                            .then(() => {
                                !!callback && callback();
                            })
                            .catch((err: ClaireError) => {
                                this.errorHandler.handle({socket}, err);
                            });
                    });
                });
                socket.on("disconnect", () => {
                    this.onSocketDisconnect(socket);
                });
            });
        });

        return server;
    }

    public getConnectedSockets(): ISocket[] {
        let sockets = this.io.sockets.connected;
        let keys = Object.keys(sockets);
        return keys.map((key) => sockets[key] as ISocket);
    }

    public onSocketConnect(socket: ISocket): void {
    }

    public onSocketDisconnect(socket: ISocket): void {
    }

}
