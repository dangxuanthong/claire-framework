import {AbstractRequestHandler} from "../AbstractRequestHandler";
import {SocketConnection} from "./SocketConnection";
import {AbstractDataValidator} from "../data_validator/AbstractDataValidator";
import {ISocket} from "./ISocket";
import {AbstractSocketMiddleware} from "./AbstractSocketMiddleware";
import {AbstractSocketController} from "./AbstractSocketController";
import {Initable} from "../../system/Initable";
import {IAppContext} from "../..";
import {AbstractSocketErrorHandler} from "../error_handler/AbstractSocketErrorHandler";
import {AbstractSocketAuthorizationProvider} from "../socket_handler/AbstractSocketAuthorizationProvider";

export abstract class AbstractSocketRequestHandler extends AbstractRequestHandler<SocketConnection> implements Initable {

    public readonly mountUrl: string;
    public readonly middleware: AbstractSocketMiddleware[];
    public readonly controllers: AbstractSocketController[];

    protected constructor(
        listenPort: number,
        dataValidator: AbstractDataValidator,
        errorHandler: AbstractSocketErrorHandler,
        middleware: (AbstractSocketMiddleware | undefined)[],
        mountUrl: string,
        controllers: (AbstractSocketController | undefined)[],
        authorizationProvider?: AbstractSocketAuthorizationProvider,
    ) {
        super(listenPort, dataValidator, errorHandler, middleware, controllers, authorizationProvider);
        this.mountUrl = mountUrl;
    }

    public async init(appContext: IAppContext): Promise<void> {
        await super.init(appContext);
        return;
    }

    public async stop(): Promise<void> {
        await super.stop();
        return;
    }

    public abstract getConnectedSockets(): ISocket[];

    public abstract onSocketConnect(socket: ISocket): void;

    public abstract onSocketDisconnect(socket: ISocket): void;

}
