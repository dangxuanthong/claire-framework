import {ISocket} from "./ISocket";

export interface SocketConnection {
    socket: ISocket;
}
