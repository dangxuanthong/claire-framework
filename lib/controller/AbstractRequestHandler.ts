import {Server} from "http";
import {IRequestHandler} from "./IRequestHandler";
import {AbstractDataValidator} from "./data_validator/AbstractDataValidator";
import {IErrorHandler} from "./error_handler/IErrorHandler";
import {IMiddleware} from "./IMiddleware";
import {Utils} from "../system/Utils";
import {IAppContext, Initable} from "..";
import {IRequestController} from "./IRequestController";
import {IAuthorizationProvider} from "./IAuthorizationProvider";

export abstract class AbstractRequestHandler<T> implements IRequestHandler {

    public listenPort: number;
    protected dataValidator: AbstractDataValidator;
    protected errorHandler: IErrorHandler<T>;
    protected middleware: IMiddleware<T>[];
    protected controllers: IRequestController<any>[];
    protected authorizationProvider?: IAuthorizationProvider<any, any>;

    protected constructor(
        listenPort: number,
        dataValidator: AbstractDataValidator,
        errorHandler: IErrorHandler<T>,
        middleware: (IMiddleware<T> | undefined)[],
        controllers: (IRequestController<any> | undefined)[],
        authorizationProvider?: IAuthorizationProvider<any, any>,
    ) {
        this.listenPort = listenPort;
        this.dataValidator = dataValidator;
        this.errorHandler = errorHandler;
        this.middleware = Utils.getCleanArray(middleware);
        this.controllers = Utils.getCleanArray(controllers);
        this.authorizationProvider = authorizationProvider;
    }

    public async init(appContext: IAppContext): Promise<void> {
        let initables: Initable[] = [];
        initables = initables.concat(this.middleware);
        initables = initables.concat(this.controllers);
        initables.push(this.errorHandler);
        initables.push(this.dataValidator);
        if (this.authorizationProvider) {
            initables.push(this.authorizationProvider);
        }
        for (let i = 0; i < initables.length; i++) {
            await initables[i].init(appContext);
        }
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract configure(server: Server): Server;

}


