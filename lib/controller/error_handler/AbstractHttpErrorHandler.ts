import {IErrorHandler} from "./IErrorHandler";
import {HttpConnection, IAppContext} from "../..";

export abstract class AbstractHttpErrorHandler implements IErrorHandler<HttpConnection> {

    protected constructor() {

    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract handle(connection: HttpConnection, error: any): void;

}
