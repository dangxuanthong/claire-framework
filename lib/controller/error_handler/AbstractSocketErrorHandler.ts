import {IErrorHandler} from "./IErrorHandler";
import {IAppContext, SocketConnection} from "../..";

export abstract class AbstractSocketErrorHandler implements IErrorHandler<SocketConnection> {

    protected constructor() {

    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract handle(connection: SocketConnection, error: any): void;

}
