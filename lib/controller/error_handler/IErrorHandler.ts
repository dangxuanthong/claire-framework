import {Initable} from "../../system/Initable";

export interface IErrorHandler<T> extends Initable {

    handle(connection: T, error: any): void;

}
