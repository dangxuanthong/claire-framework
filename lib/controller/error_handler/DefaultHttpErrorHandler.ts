import {AbstractHttpErrorHandler} from "./AbstractHttpErrorHandler";
import {ClaireError, ILogger, HttpConnection, IAppContext, Initable} from "../..";
import {Errors} from "../../system/Errors";

export class DefaultHttpErrorHandler extends AbstractHttpErrorHandler implements Initable {

    private logger: ILogger;

    public constructor() {
        super();
    }

    public async init(appContext: IAppContext): Promise<void> {
        this.logger = appContext.getLogger();
    }

    public handle(connection: HttpConnection, err: any): void {
        this.logger.error(connection.request.method, connection.request.url);
        if (err instanceof ClaireError) {
            this.logger.error(err.name, err.message);
            connection.response.json({
                success: false,
                result: err,
            });
        } else {
            this.logger.error(err.name, err.message);
            connection.response.json({
                success: false,
                result: new ClaireError(Errors.system.INTERNAL_SYSTEM_ERROR),
            });
        }
    }
}
