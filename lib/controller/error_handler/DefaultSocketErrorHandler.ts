import {AbstractSocketErrorHandler} from "./AbstractSocketErrorHandler";
import {IAppContext, ILogger, SocketConnection} from "../..";

export class DefaultSocketErrorHandler extends AbstractSocketErrorHandler {

    private logger: ILogger;

    public constructor() {
        super();
    }

    public async init(appContext: IAppContext): Promise<void> {
        this.logger = appContext.getLogger();
    }

    public handle(connection: SocketConnection, err: any): void {
        this.logger.error(err.name, err.message);
        connection.socket.disconnect(true);
    }

}
