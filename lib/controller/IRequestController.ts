import {Initable} from "../system/Initable";

export interface IRequestController<T> extends Initable {

    getHandlers(): T[];

}
