import {AbstractService} from "..";

export interface IServiceProvider {

    get<S extends AbstractService>(serviceClass: any): S;

}
