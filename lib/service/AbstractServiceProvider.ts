import {AbstractService, IServiceProvider} from "..";

export abstract class AbstractServiceProvider implements IServiceProvider {

    protected constructor() {

    }

    public abstract get<S extends AbstractService>(serviceClass: any): S;

}
