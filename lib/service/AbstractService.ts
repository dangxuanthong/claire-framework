import {IService} from "./IService";
import {IAppContext, IDatabaseAdapter} from "..";

export abstract class AbstractService implements IService {

    protected databaseAdapter: IDatabaseAdapter;

    protected constructor() {

    }

    public async init(appContext: IAppContext): Promise<void> {
        this.databaseAdapter = appContext.getDatabaseAdapter();
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

}

