import {AbstractServiceProvider} from "./AbstractServiceProvider";
import {AbstractService} from "./AbstractService";
import {Errors} from "../system/Errors";
import {ClaireError} from "..";

export class DefaultServiceProvider extends AbstractServiceProvider {

    private services: AbstractService[];

    public constructor(services: AbstractService[]) {
        super();
        this.services = services;
    }

    public get<S extends AbstractService>(serviceClass: any): S {
        let service = this.services.find(s => s instanceof serviceClass);
        if (!service) {
            throw new ClaireError(Errors.system.SERVICE_NOT_FOUND, serviceClass.name);
        }
        return service as S;
    }

}
