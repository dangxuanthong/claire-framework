import {AbstractCacheAdapter, IAppContext} from "..";

export class DefaultInMemoryCacheAdapter extends AbstractCacheAdapter {

    private hashmap: Map<string, any>;
    private countdownArray: Array<{ key: string, expireSecond: number }>;

    public constructor() {
        super();
        this.hashmap = new Map<string, string>();
        this.countdownArray = [];
    }

    public async init(appContext: IAppContext): Promise<void> {
        setInterval(() => {
            for (let i = this.countdownArray.length - 1; i >= 0; i--) {
                let e = this.countdownArray[i];
                e.expireSecond -= 1;
                if (e.expireSecond <= 0) {
                    this.hashmap.delete(e.key);
                    this.countdownArray = [
                        ...this.countdownArray.slice(0, i),
                        ...this.countdownArray.slice(i + 1),
                    ]
                }
            }
        }, 1000);
        return;
    }

    public isPersistent(): boolean {
        return false;
    }

    public async getCache<T extends any>(key: string): Promise<T> {
        return this.hashmap.get(key) as T;
    }

    public async setCache<T extends any>(key: string, value: T, expireAfterSecond?: number): Promise<void> {
        this.hashmap.set(key, value);
        if (!!expireAfterSecond) {
            let index = this.countdownArray.findIndex((e) => e.key === key);
            if (index >= 0) {
                this.countdownArray[index].expireSecond = expireAfterSecond;
            } else {
                this.countdownArray.push({key, expireSecond: expireAfterSecond});
            }
        }
        return;
    }

    public async flush(): Promise<void> {
        this.hashmap = new Map<string, string>();
        this.countdownArray = [];
        return;
    }

}
