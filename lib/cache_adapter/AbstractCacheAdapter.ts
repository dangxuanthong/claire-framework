import {Initable, ICacheAdapter, IAppContext} from "..";

export abstract class AbstractCacheAdapter implements ICacheAdapter, Initable {

    protected constructor() {

    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract isPersistent(): boolean;

    public abstract flush(): Promise<void>;

    public abstract setCache<T extends any>(key: string, value: T, expireAfter?: number): Promise<void>;

    public abstract getCache<T>(key: string): Promise<T>;

}
