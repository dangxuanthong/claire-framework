import redis = require("redis");
import {Errors} from "../system/Errors";
import {ClaireError} from "..";
import {AbstractCacheAdapter} from "./AbstractCacheAdapter";

export class DefaultRedisCacheAdapter extends AbstractCacheAdapter {

    private readonly serverUrl: string;
    private readonly namespace: string;

    private redisClient: any;

    public constructor(serverUrl: string, namespace: string) {
        super();
        this.serverUrl = serverUrl;
        this.namespace = namespace;
    }

    public isPersistent(): boolean {
        return true;
    }

    public async init(): Promise<void> {
        this.redisClient = redis.createClient(this.serverUrl, {
            prefix: this.namespace,
        });
        return await new Promise<void>((resolve, reject) => {
            this.redisClient.on("connect", (err: any) => {
                if (err) {
                    return reject(new ClaireError(Errors.system.CONNECTION_ERROR, err.stack || String(err)));
                }
                return resolve();
            });
        });
    }

    public async setCache<T extends any>(key: string, value: T, expireAfterSecond?: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            if (!!expireAfterSecond) {
                this.redisClient.set(key, JSON.stringify(value), "EX", expireAfterSecond, (err: any) => {
                    if (err) {
                        return reject(new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err)));
                    }
                    return resolve();
                });
            } else {
                this.redisClient.set(key, JSON.stringify(value), (err: any) => {
                    if (err) {
                        return reject(new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err)));
                    }
                    return resolve();
                });
            }
        });
    }

    public getCache<T extends any>(key: string): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.redisClient.get(key, (err: any, reply: string) => {
                if (err || (reply === null)) {
                    return reject(new ClaireError(Errors.database.QUERY_ERROR));
                }
                return resolve(JSON.parse(reply) as T);
            });
        });
    }

    public flush(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.redisClient.flushall("ASYNC", (err: any) => {
                if (!!err) {
                    reject(err);
                }
                resolve();
            });
        });
    }

}
