export interface ICacheAdapter {

    isPersistent(): boolean;

    flush(): Promise<void>;

    setCache<T extends any>(key: string, value: T, expireAfter?: number): Promise<void>;

    getCache<T extends any>(key: string): Promise<T>;

}
