import "reflect-metadata";

export {Claire} from "./system/Claire";
export {ClaireBuilder} from "./system/ClaireBuilder";
export {IAppContext} from "./system/IAppContext";

//-- necessary interfaces
export {IEnvProvider} from "./env_provider/IEnvProvider";
export {ILogger, LogLimit} from "./logger/ILogger";
export {ICacheAdapter} from "./cache_adapter/ICacheAdapter";
export {IServiceProvider} from "./service/IServiceProvider";
export {IHttpRequest} from "./controller/http_handler/IHttpRequest";
export {IHttpResponse} from "./controller/http_handler/IHttpResponse";
export {ISocketProvider} from "./controller/socket_handler/ISocketProvider";
export {IMessagingAdapter} from "./messaging_adapter/IMessagingAdapter";
export {Initable} from "./system/Initable";
export {IQuery} from "./database_adapter/IQuery";
export {IDatabaseAdapter} from "./database_adapter/IDatabaseAdapter";
export {ISocket} from "./controller/socket_handler/ISocket";
export {ITransaction} from "./database_adapter/ITransaction";
export {IQueryProvider} from "./database_adapter/IQueryProvider";

//-- necessary abstract class
export {AbstractModel} from "./model/AbstractModel";
export {AbstractEnvProvider} from "./env_provider/AbstractEnvProvider";
export {AbstractCacheAdapter} from "./cache_adapter/AbstractCacheAdapter";
export {AbstractDatabaseAdapter} from "./database_adapter/AbstractDatabaseAdapter";
export {AbstractService} from "./service/AbstractService";
export {AbstractLogger} from "./logger/AbstractLogger";
export {SqlProvider, NoSqlProvider} from "./database_adapter/AbstractDatabaseAdapter";
export {AbstractHttpResponder} from "./controller/responder/AbstractHttpResponder";
export {AbstractSocketController} from "./controller/socket_handler/AbstractSocketController";
export {AbstractSocketMiddleware} from "./controller/socket_handler/AbstractSocketMiddleware";
export {AbstractHttpController, HTTP} from "./controller/http_handler/AbstractHttpController";
export {AbstractHttpMiddleware} from "./controller/http_handler/AbstractHttpMiddleware";
export {AbstractMessagingAdapter} from "./messaging_adapter/AbstractMessagingAdapter";
export {AbstractHttpAuthorizationProvider} from "./controller/http_handler/AbstractHttpAuthorizationProvider";
export {AbstractSocketAuthorizationProvider} from "./controller/socket_handler/AbstractSocketAuthorizationProvider";

//-- export types & enums
export {ClaireError} from "./system/ClaireError";
export {ConsoleLogMedia} from "./logger/log_media/ConsoleLogMedia";
export {FileLogMedia} from "./logger/log_media/FileLogMedia";
export {SocketConnection} from "./controller/socket_handler/SocketConnection";
export {HttpConnection} from "./controller/http_handler/HttpConnection";
export {ChannelType} from "./messaging_adapter/IMessagingAdapter";

export {Charset} from "./model/ModelMetaData";
export {DataType} from "./model/FieldMetaData";
export {
    eq,
    neq,
    belongs,
    contains,
    gt,
    gte,
    lt,
    lte,
    regex,
} from "./database_adapter/QueryOperator";

//-- data adapter
export {
    //-- table prototype
    DataTable,
    //-- field constraints
    PrimaryKey,
    ForeignKey,
    Unique,
    Default,
    Nullable,
    DataField,
    AutoGen,
    GetSet,
} from "./model/TableMapper";

export {
    EnvTemplate,
    EnvVar
}
    from "./env_provider/EnvMapper";

//-- controller
export {
    Mapping,
    Override,
    OpenAccess,
    Validator,
    Permission,
    PermissionGroup,
    PermissionName,
    ControllerMetadata,
    ControllerHandlerMetadata
} from "./controller/http_handler/ControllerMetadata";
export {Message, SocketChannelHandler} from "./controller/socket_handler/SocketChannelHandler";

export {
    Validate,
    Required,
    Optional,
    Match,
    GreaterThan,
    LessThan,
    IsInteger,
    InSet,
    Range,
    IsArray,
    SubSet,
    DefaultValue,
} from "./controller/data_validator/ValidationRules";

export {
    IAccessCondition, AbstractAccessCondition, ConditionValueType, ConditionPrototype, PermissionType
} from "./controller/http_handler/RBAC";

//- default implementations
export {DefaultLogger} from "./logger/DefaultLogger";
export {DefaultEnvProvider} from "./env_provider/DefaultEnvProvider";
export {DefaultSqlAdapter} from "./database_adapter/DefaultSqlAdapter";
export {DefaultNoSqlAdapter} from "./database_adapter/DefaultNoSqlAdapter";
export {DefaultRedisCacheAdapter} from "./cache_adapter/DefaultRedisCacheAdapter";
export {DefaultJsonResponder} from "./controller/responder/DefaultJsonResponder";
export {DefaultFileResponder} from "./controller/responder/DefaultFileResponder";
export {DefaultDataValidator} from "./controller/data_validator/DefaultDataValidator";
export {DefaultHttpRequestHandler} from "./controller/http_handler/DefaultHttpRequestHandler";
export {DefaultSocketRequestHandler} from "./controller/socket_handler/DefaultSocketRequestHandler";
export {DefaultHttpErrorHandler} from "./controller/error_handler/DefaultHttpErrorHandler";
export {DefaultSocketErrorHandler} from "./controller/error_handler/DefaultSocketErrorHandler";
export {DefaultRedisMessagingAdapter} from "./messaging_adapter/DefaultRedisMessagingAdapter";
export {DefaultHttpResourceController} from "./controller/http_handler/DefaultHttpResourceController";
export {BodyParser} from "./controller/http_handler/BodyParser";
export {CORS} from "./controller/http_handler/CORS";
