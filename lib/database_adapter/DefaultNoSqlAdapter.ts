import mongoose, {Schema} from "mongoose";
import {
    AbstractDatabaseAdapter, AbstractModel,
    ClaireError,
    DataType,
    eq,
    IAppContext,
    ILogger,
    ITransaction,
} from "..";
import {IQuery, QueryCondition, SubKeySet} from "./IQuery";
import {Errors} from "../system/Errors";
import {ModelMetaData} from "../model/ModelMetaData";
import {Operator} from "./QueryOperator";
import {Utils} from "../system/Utils";
import {NoSqlProvider} from "./AbstractDatabaseAdapter";
import {AbstractQuery} from "./AbstractQuery";

interface SequenceProvider {
    getNextSequence(tableName: string): Promise<number>;
}

class ModelAdapter<T> extends AbstractQuery<T> {

    private readonly sequenceProvider: SequenceProvider;
    private readonly session?: any;

    constructor(
        model: new(...args: any[]) => T,
        metadata: ModelMetaData,
        schema: { modelConnection: any },
        sequenceProvider: SequenceProvider,
        session?: any,
    ) {
        super(model, metadata, schema);
        this.sequenceProvider = sequenceProvider;
        this.session = session;
    }

    private getQueryOptions(): any {
        const opt: any = {};
        if (this.session) {
            opt.session = this.session;
        }
        return opt;
    }

    public async saveOne(modelInstance: Partial<T>): Promise<T> {
        //-- use mongoose create function
        let result = await this.saveMany([modelInstance]);
        return result[0];
    }

    public async saveMany(modelInstances: Partial<T>[]): Promise<T[]> {
        //-- use mongoose bulkWrite function
        let result = await this.currentSchema.modelConnection.insertMany(this.convertToDataObjects(modelInstances), this.getQueryOptions());
        if (!result || result.length !== modelInstances.length) {
            throw new ClaireError(Errors.database.BULK_OPERATION_ERROR);
        }
        let instances: T[] = [];
        let primaryKey = this.currentMetadata.getPrimaryKey();

        try {
            if (primaryKey.isAutoIncrement) {
                let operations: any[] = [];
                let sequences: any[] = [];

                if (primaryKey.dataType === DataType.INTEGER) {
                    //-- update sequential ids for inserted records
                    for (let i = 0; i < result.length; i++) {
                        let id = await this.sequenceProvider.getNextSequence(this.currentMetadata.tableName!);
                        operations.push({
                            updateOne: {
                                filter: {_id: result[i]["_id"]},
                                update: {[primaryKey.fieldDataName!]: id}
                            }
                        });
                        sequences.push(id);
                    }
                } else {
                    //-- up
                    for (let i = 0; i < result.length; i++) {
                        let id = result[i]["_id"];
                        operations.push({
                            updateOne: {
                                filter: {_id: id},
                                update: {[primaryKey.fieldDataName!]: id}
                            }
                        });
                        sequences.push(id);
                    }
                }
                //-- call bulkWrite to update ids
                await this.currentSchema.modelConnection.bulkWrite(operations, this.getQueryOptions());
                //-- update primary key value in returned values
                for (let i = 0; i < modelInstances.length; i++) {
                    // @ts-ignore
                    instances.push(Object.assign({}, this.defaultLogicValues, modelInstances[i], {[primaryKey.fieldLogicName!]: sequences[i]}));
                }
            } else {
                for (let i = 0; i < modelInstances.length; i++) {
                    // @ts-ignore
                    instances.push(modelInstances[i]);
                }
            }
            return instances;
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err));
        }
    }

    public async getOne(queries?: QueryCondition<T>[], options?: { projection?: SubKeySet<T>[] }): Promise<T | undefined> {

        let queryObject = this.getQueryObjectFromQueryConditions(queries || []);

        let query = this.currentSchema.modelConnection.findOne(queryObject, undefined, this.getQueryOptions());

        if (options && options.projection) {
            let projection = this.getProjectionFields(options.projection).reduce((collector, fieldName) => ({
                ...collector,
                [fieldName]: 1
            }), {"_id": 0});
            query = query.select(projection);
        }

        let instance = await query.lean().exec();
        if (!instance) {
            return undefined;
        }

        return this.convertToLogicObjects([instance], options && options.projection)[0];
    }

    public async getMany(queries?: QueryCondition<T>[], options?: {
        projection?: SubKeySet<T>[], limit?: number, page?: number
    }): Promise<T[]> {
        let rawQuery = this.getQueryObjectFromQueryConditions(queries || [],);
        //-- use mongoose find function
        let promise = this.currentSchema.modelConnection.find(rawQuery, undefined, this.getQueryOptions());

        if (options && options.limit) {
            promise = promise.limit(options.limit);
            if (options.page) {
                //-- page in mongoose starts with 0
                promise = promise.skip(options.limit * (options.page - 1));
            }
        }

        if (options && options.projection) {
            let projection = options.projection.reduce((collector, fieldName) => ({
                ...collector,
                [fieldName]: 1
            }), {"_id": 0});
            promise = promise.select(projection);
        }

        let result = await promise.lean().exec();

        return this.convertToLogicObjects(result, options && options.projection);
    }

    public async deleteOne(modelInstance: T): Promise<T> {
        let primaryKey = this.currentMetadata.getPrimaryKey();

        //-- construct query object
        // @ts-ignore
        let queryObject = this.getQueryObjectFromQueryConditions([{[primaryKey.fieldDataName!]: eq(modelInstance[primaryKey.fieldDataName!])}]);
        try {
            await this.currentSchema.modelConnection.findOneAndRemove(queryObject, this.getQueryOptions());
            return modelInstance;
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || err);
        }
    }

    public async deleteMany(queries: QueryCondition<T>[]): Promise<number> {

        let rawQuery = this.getQueryObjectFromQueryConditions(queries);
        //-- use mongoose deleteMany function
        try {
            let result = await this.currentSchema.modelConnection.deleteMany(rawQuery, this.getQueryOptions());
            return result["deletedCount"];
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err));
        }
    }

    public async updateOne(modelInstance: T): Promise<T> {
        try {

            await this.currentSchema.modelConnection.findOneAndUpdate(
                // @ts-ignore
                {[this.currentMetadata.getPrimaryKey().fieldDataName]: modelInstance[this.currentMetadata.getPrimaryKey().fieldDataName!]},
                // @ts-ignore
                this.convertToDataObjects(modelInstance), this.getQueryOptions());
            return modelInstance;
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || err);
        }
    }

    public async updateMany(queries: QueryCondition<T>[], update: Partial<T>): Promise<number> {

        let rawQuery = this.getQueryObjectFromQueryConditions(queries);
        //-- use mongoose updateMany function
        try {
            let result = await this.currentSchema.modelConnection.updateMany(rawQuery, update, this.getQueryOptions());
            return result["ok"];
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err));
        }
    }

    public rawQuery(query: any): Promise<any[]> {
        throw new ClaireError(Errors.system.NOT_IMPLEMENTED);
    }

    protected getQueryObjectFromQueryConditions(queries: QueryCondition<T>[]): any {

        let result: any = {};
        if (!!queries && queries.length) {
            for (let i = 0; i < queries.length; i++) {
                let query = queries[i];
                Object.keys(query).forEach((key: string) => {
                    // @ts-ignore
                    let operator = query[key];
                    let dataKey = this.currentMetadata.getFieldByLogicName(key)!.fieldDataName!;
                    switch (operator.operator) {
                        case Operator.COMMON_EQUALITY:
                            result[dataKey] = {$eq: operator.value};
                            break;
                        case Operator.COMMON_INEQUALITY:
                            result[dataKey] = {$ne: operator.value};
                            break;
                        case Operator.COMMON_BELONG:
                            result[dataKey] = {$in: operator.value};
                            break;
                        case Operator.STRING_CONTAIN:
                            result[dataKey] = {
                                $regex: new RegExp(`.*${Utils.escapeRegExp(operator.value)}.*`),
                            };
                            break;
                        case Operator.STRING_REGEX:
                            result[dataKey] = {
                                $regex: new RegExp(operator.value),
                            };
                            break;
                        case Operator.NUMBER_GT:
                            result[dataKey] = {$gt: operator.value};
                            break;
                        case Operator.NUMBER_GTE:
                            result[dataKey] = {$gte: operator.value};
                            break;
                        case Operator.NUMBER_LT:
                            result[dataKey] = {$lt: operator.value};
                            break;
                        case Operator.NUMBER_LTE:
                            result[dataKey] = {$lte: operator.value};
                            break;
                    }
                });
            }
        }
        return result;
    }
}

class TransactionAdapter implements ITransaction {

    private readonly sequenceProvider: SequenceProvider;
    private readonly session: any;
    private readonly modelAdapters: Map<string, ModelAdapter<any>>;
    private readonly modelMetadata: ModelMetaData[];
    private readonly schemas: { [name: string]: { modelConnection: any } };

    public constructor(session: any, sequenceProvider: SequenceProvider, modelMetaData: ModelMetaData[], schemas: { [name: string]: { modelConnection: any } }) {
        this.session = session;
        this.sequenceProvider = sequenceProvider;
        this.modelAdapters = new Map();
        this.modelMetadata = modelMetaData;
        this.schemas = schemas;
    }

    public use<T extends AbstractModel>(model: { new(...args: any[]): T }): IQuery<T> {
        let modelAdapter = this.modelAdapters.get(model.name);
        if (!modelAdapter) {
            modelAdapter = new ModelAdapter<T>(model, this.modelMetadata.find((meta) => meta.modelName === model.name)!, this.schemas[model.name], this.sequenceProvider, this.session);
            this.modelAdapters.set(model.name, modelAdapter);
        }
        return modelAdapter;
    }

    public async commit(): Promise<void> {
        return this.session.commitTransaction();
    }

    public async rollback(): Promise<void> {
        return this.session.abortTransaction();
    }

}

export class DefaultNoSqlAdapter extends AbstractDatabaseAdapter implements SequenceProvider {

    private readonly connectionString: string;
    private readonly modelAdapters: Map<string, ModelAdapter<any>>;
    private readonly provider: NoSqlProvider;

    private logger: ILogger;
    private connection: any;
    private counterSchema: any;

    public constructor(provider: NoSqlProvider, connectionString: string, models: typeof AbstractModel[]) {
        super(models);
        this.connectionString = connectionString;
        this.modelAdapters = new Map();
        this.provider = provider;
    }

    public async init(appContext: IAppContext): Promise<void> {
        await super.init(appContext);
        //-- set logger
        this.logger = appContext.getLogger();

        //-- connect to mongodb
        mongoose.set("useNewUrlParser", true);
        mongoose.set("useFindAndModify", false);
        mongoose.set("useCreateIndex", true);
        mongoose.set("useUnifiedTopology", true);
        this.logger.debug('Connecting to mongodb...');
        let result = await new Promise<true | ClaireError>(resolve => {
            mongoose.connect(`mongodb://${this.connectionString}`)
                .then((mongo: any) => {
                    this.connection = mongo;
                    return resolve(true);
                })
                .catch((err: any) => {
                    return resolve(new ClaireError("INTERNAL_SYSTEM_ERROR", err.stack || String(err)));
                });
        });
        if (result instanceof ClaireError) {
            throw result;
        }

        this.logger.debug('Generating in-memory schemas...');
        //-- create in-memory schemas
        for (let i = 0; i < this.models.length; i++) {
            let model = this.models[i];
            let meta = this.modelMetadata.find((meta) => meta.modelName === model.name);
            if (!meta) {
                throw new ClaireError(Errors.database.MODEL_METADATA_NOT_FOUND, model.name);
            }
            //-- inject modelConnection
            this.schemas[model.name].modelConnection = this.connection.model(model.name, this.getSchemaFromPrototype(meta), meta.tableName);
        }
        //-- build counter schema
        let counterSchema: any = {
            tableName: {
                type: String,
            },
            counter: {
                type: Number,
            },
        };
        this.counterSchema = this.connection.model("_counter", counterSchema, "_counter");
    }

    public async stop(): Promise<void> {
        this.logger.debug("Disconnecting from database...");
        this.connection.disconnect();
        await super.stop();
    }

    private getSchemaFromPrototype(prototype: ModelMetaData): any {
        let result: any = {};
        prototype.getFields().forEach((field) => {

            let fieldProperties: any = {};
            //-- default is not null
            Object.assign(fieldProperties, {required: !field.nullable});

            if (field.isPrimaryKey) {
                if (!field.isAutoIncrement) {
                    //-- if not auto then must be unique
                    Object.assign(fieldProperties, {unique: true});
                }
                //-- allow empty for insert later
                Object.assign(fieldProperties, {required: false, sparse: true});
            }
            if (field.isForeignKey === true) {
                let referModel = this.modelMetadata.find((meta) => meta.modelName === field.referModel);
                Object.assign(fieldProperties, {
                    type: mongoose.Types.ObjectId,
                    ref: referModel && referModel.tableName,
                    required: true
                });
            }
            if (field.isUnique === true) {
                Object.assign(fieldProperties, {unique: true});
                if (field.nullable) {
                    Object.assign(fieldProperties, {sparse: true});
                }
            }

            if (field.defaultValue !== undefined) {
                Object.assign(fieldProperties, {default: field.defaultValue});
            }
            switch (field.dataType) {
                case DataType.TEXT:
                case DataType.STRING:
                    Object.assign(fieldProperties, {type: Schema.Types.String});
                    break;
                case DataType.INTEGER:
                case DataType.FLOAT:
                    Object.assign(fieldProperties, {type: Schema.Types.Number});
                    break;
                case DataType.BOOL:
                    Object.assign(fieldProperties, {type: Schema.Types.Boolean});
                    break;
                case DataType.ARRAY:
                    Object.assign(fieldProperties, {type: Schema.Types.Mixed});
                    break;
            }
            result[field.fieldDataName!] = fieldProperties;
        });
        return result;
    }

    public use<T extends AbstractModel>(model: { new(...args: any[]): T }): IQuery<T> {
        let modelAdapter = this.modelAdapters.get(model.name);
        if (!modelAdapter) {
            modelAdapter = new ModelAdapter<T>(model, this.modelMetadata.find((meta) => meta.modelName === model.name)!, this.schemas[model.name], this);
            this.modelAdapters.set(model.name, modelAdapter);
        }
        return modelAdapter;
    }

    public async getNextSequence(tableName: string): Promise<number> {
        try {
            let result = await this.counterSchema.findOneAndUpdate(
                {tableName},
                {$inc: {counter: 1}},
                {new: true, upsert: true, rawResult: true},
            );
            return result["value"]["counter"];
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err));
        }
    }

    public async createTransaction(): Promise<ITransaction> {
        let session = await this.connection.startSession();
        session.startTransaction();
        return new TransactionAdapter(
            session, this, this.modelMetadata, this.schemas
        );
    }
}
