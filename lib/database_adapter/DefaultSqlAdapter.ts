import fs = require("fs");
import path = require("path");
import {exec} from "child_process";
import {Sequelize, Op, INTEGER, TEXT, STRING, BOOLEAN} from "sequelize";
import {AbstractDatabaseAdapter, SqlProvider} from "./AbstractDatabaseAdapter";
import {Errors} from "../system/Errors";
import {Utils} from "../system/Utils";
import {ModelMetaData} from "../model/ModelMetaData";
import {DataType, FieldMetaData} from "../model/FieldMetaData";
import {AbstractModel, Charset, ClaireError, eq, IAppContext, ILogger, ITransaction,} from "..";
import {IQuery, QueryCondition, SubKeySet} from "./IQuery";
import {Operator} from "./QueryOperator";
import {AbstractQuery} from "./AbstractQuery";

type ModelMetadataDifference = { added: ModelMetaData | undefined, removed: ModelMetaData | undefined };

class ModelAdapter<T> extends AbstractQuery<T> {

    private readonly connection: Sequelize;
    private readonly transaction: any;

    constructor(connection: Sequelize, model: new (...args: any[]) => AbstractModel, metadata: ModelMetaData, schema: { modelConnection: any }, transaction?: any) {
        super(model, metadata, schema);
        this.connection = connection;
        this.transaction = transaction;
    }

    public async saveOne(modelInstance: Partial<T>): Promise<T> {
        let result = await this.saveMany([modelInstance]);
        return result[0];
    }

    public async saveMany(modelInstances: Partial<T>[]): Promise<T[]> {

        let primaryKey = this.currentMetadata.getPrimaryKey();

        //-- use sequelize bulkCreate function
        let instances = await this.currentSchema.modelConnection.bulkCreate(this.convertToDataObjects(modelInstances), {transaction: this.transaction});
        if (instances.length !== modelInstances.length) {
            throw new ClaireError(Errors.database.BULK_OPERATION_ERROR);
        }
        let result: T[] = [];
        if (primaryKey.isAutoIncrement) {
            for (let i = 0; i < instances.length; i++) {
                result.push(Object.assign({}, this.defaultLogicValues, modelInstances[i], {[primaryKey.fieldLogicName!]: instances[i][primaryKey.fieldDataName!]}) as T);
            }
        } else {
            for (let i = 0; i < instances.length; i++) {
                result.push(Object.assign({}, this.defaultLogicValues, modelInstances[i]) as T);
            }
        }
        return result;
    }

    public async getOne(queries?: QueryCondition<T>[], options?: { projection?: SubKeySet<T>[] }): Promise<T | undefined> {

        let conditions = {
            where: this.getQueryObjectFromQueryConditions(queries || []),
            transaction: this.transaction,
        };

        if (options && options.projection) {
            Object.assign(conditions, {
                attributes: this.getProjectionFields(options.projection),
            });
        }

        let instance = await this.currentSchema.modelConnection.findOne(conditions);
        if (!instance) {
            return undefined;
        }
        return this.convertToLogicObjects([instance], options && options.projection)[0];
    }

    public async getMany(queries: QueryCondition<T>[], options?: {
        projection?: SubKeySet<T>[],
        limit?: number,
        page?: number,
    }): Promise<T[]> {
        let conditions = {
            where: this.getQueryObjectFromQueryConditions(queries),
            transaction: this.transaction
        };

        if (options && options.limit) {
            conditions = Object.assign(conditions, {limit: options.limit});
            if (options.page) {
                conditions = Object.assign(conditions, {offset: options.limit * (options.page - 1)});
            }
        }

        if (options && options.projection) {
            Object.assign(conditions, {
                attributes: this.getProjectionFields(options.projection),
            });
        }

        let instances = await this.currentSchema.modelConnection.findAll(conditions);
        return this.convertToLogicObjects(instances, options && options.projection);
    }

    public async deleteOne(modelInstance: T): Promise<T> {
        let primaryKey = this.currentMetadata.getPrimaryKey();

        try {
            // @ts-ignore
            await this.deleteMany([{[primaryKey.fieldDataName!]: eq(modelInstance[primaryKey.fieldDataName!])}]);
            return modelInstance;
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err));
        }
    }

    public async deleteMany(queries: QueryCondition<T>[]): Promise<number> {
        try {
            return await this.currentSchema.modelConnection.destroy({
                where: this.getQueryObjectFromQueryConditions(queries),
                transaction: this.transaction
            });
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err));
        }
    }

    public async updateOne(modelInstance: T): Promise<T> {
        try {
            await this.currentSchema.modelConnection.update(modelInstance, {
                // @ts-ignore
                where: this.getQueryObjectFromQueryConditions([{[this.currentMetadata.getPrimaryKey().fieldDataName!]: eq(modelInstance[this.currentMetadata.getPrimaryKey().fieldLogicName!])}]),
                transaction: this.transaction
            });
            return modelInstance;
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err));
        }
    }

    public async updateMany(queries: QueryCondition<T>[], update: Partial<T>): Promise<number> {
        //-- remove undefined fields from update because sequelize treat undefined as null
        try {
            let queryObject = this.getQueryObjectFromQueryConditions(queries);
            let result = await this.currentSchema.modelConnection.update(this.convertToDataObjects([update])[0], {
                where: queryObject,
                transaction: this.transaction
            });
            if (result[0] === 0) {
                //-- may the updated value does not update anything
                let result = await this.currentSchema.modelConnection.findAndCountAll({
                    where: Object.assign({}, update, queryObject),
                    transaction: this.transaction
                });
                return result.count;
            }
            return result[0];
        } catch (err) {
            throw new ClaireError(Errors.database.QUERY_ERROR, err.stack || String(err));
        }
    }

    public rawQuery(query: any): Promise<any> {
        return new Promise<any[]>((resolve) => {
            this.connection.query(query, {transaction: this.transaction}).then(([results, metadata]) => {
                return resolve([results, metadata]);
            })
        });
    }

    protected getQueryObjectFromQueryConditions(queries: QueryCondition<T>[]): any {
        let result: any = {};
        if (!!queries && queries.length) {
            for (let i = 0; i < queries.length; i++) {
                let query = queries[i];
                Object.keys(query).forEach((key: string) => {
                    // @ts-ignore
                    let operator = query[key];
                    let dataKey = this.currentMetadata.getFieldByLogicName(key)!.fieldDataName!;
                    switch (operator.operator) {
                        case Operator.COMMON_EQUALITY:
                            result[dataKey] = {[Op.eq]: operator.value};
                            break;
                        case Operator.COMMON_INEQUALITY:
                            result[dataKey] = {[Op.ne]: operator.value};
                            break;
                        case Operator.COMMON_BELONG:
                            result[dataKey] = {[Op.in]: operator.value};
                            break;
                        case Operator.STRING_CONTAIN:
                            result[dataKey] = {[Op.contains]: operator.value};
                            break;
                        case Operator.STRING_REGEX:
                            result[dataKey] = {[Op.regexp]: operator.value};
                            break;
                        case Operator.NUMBER_GT:
                            result[dataKey] = {[Op.gt]: operator.value};
                            break;
                        case Operator.NUMBER_GTE:
                            result[dataKey] = {[Op.gte]: operator.value};
                            break;
                        case Operator.NUMBER_LT:
                            result[dataKey] = {[Op.lt]: operator.value};
                            break;
                        case Operator.NUMBER_LTE:
                            result[dataKey] = {[Op.lte]: operator.value};
                            break;
                    }
                });
            }
        }
        return result;
    }

}

class TransactionAdapter implements ITransaction {

    private readonly connection: Sequelize;
    private readonly modelMetadata: ModelMetaData[];
    private readonly schemas: { [name: string]: { modelConnection: any } };
    private readonly transaction: any;
    private modelAdapters: Map<string, ModelAdapter<any>>;

    public constructor(
        transaction: any,
        connection: Sequelize,
        modelMetadata: ModelMetaData[],
        schemas: { [name: string]: { modelConnection: any } },
    ) {
        this.modelAdapters = new Map();
        this.transaction = transaction;
        this.connection = connection;
        this.modelMetadata = modelMetadata;
        this.schemas = schemas;
    }

    public use<T extends AbstractModel>(model: { new(...args: any[]): T }): IQuery<T> {
        let modelAdapter = this.modelAdapters.get(model.name);
        if (!modelAdapter) {
            modelAdapter = new ModelAdapter<T>(this.connection, model, this.modelMetadata.find((meta) => meta.modelName === model.name)!, this.schemas[model.name], this.transaction);
            this.modelAdapters.set(model.name, modelAdapter);
        }
        return modelAdapter;
    }

    public async commit(): Promise<void> {
        return this.transaction.commit();
    }

    public async rollback(): Promise<void> {
        return this.transaction.rollback();
    }

}

export class DefaultSqlAdapter extends AbstractDatabaseAdapter {

    private static readonly SERIAL_PROMISE = ".reduce((promise, func) => promise.then((result) => func.then(Array.prototype.concat.bind(result))), Promise.resolve([]));";
    private static readonly COMMENT_BLOCK_REGEX_PATTERN = "/\\*(\\*(?!/)|[^*])*\\*\\/";
    private static readonly SEQUELIZE_CLI_PATH = "../../node_modules/";
    // private static readonly SEQUELIZE_CLI_PATH = "../../../";

    private readonly connectionString: string;
    private readonly migrationDirectory?: string;
    private readonly provider: string;
    private readonly databaseURL: string;
    private readonly connection: Sequelize;
    private readonly modelAdapters: Map<string, ModelAdapter<any>>;

    private logger: ILogger;

    public constructor(sqlProvider: SqlProvider, connectionString: string, models: typeof AbstractModel[], migrationDirectory: string | undefined) {
        super(models);

        this.connectionString = connectionString;
        this.migrationDirectory = migrationDirectory;

        switch (sqlProvider) {
            case SqlProvider.MY_SQL:
                this.provider = "mysql";
                break;
            case SqlProvider.POSTGRES:
                this.provider = "postgres";
                break;
            default:
                throw new ClaireError(Errors.database.SQL_PROVIDER_NOT_SUPPORTED);
        }

        this.databaseURL = `${this.provider}://${this.connectionString}`;

        this.connection = new Sequelize(this.databaseURL, {
            logging: false,
            define: {
                timestamps: false
            }
        });

        this.modelAdapters = new Map();
    }

    public use<T extends AbstractModel>(model: { new(...args: any[]): T }): IQuery<T> {
        let modelAdapter = this.modelAdapters.get(model.name);
        if (!modelAdapter) {
            let modelMetaData = this.modelMetadata.find((meta) => meta.modelName === model.name);
            if (!modelMetaData) {
                throw new ClaireError(Errors.database.MODEL_METADATA_NOT_FOUND, model.name);
            }
            modelAdapter = new ModelAdapter<T>(this.connection, model, modelMetaData, this.schemas[model.name]);
            this.modelAdapters.set(model.name, modelAdapter);
        }
        return modelAdapter;
    }

    public async init(appContext: IAppContext): Promise<void> {
        await super.init(appContext);
        this.logger = appContext.getLogger();
        this.logger.debug("Init database adapter");

        //-- create db if not existed
        this.logger.debug("Create database if not exist...");
        await new Promise<void>((resolve, reject) => {
            exec(
                `${path.join(__dirname, DefaultSqlAdapter.SEQUELIZE_CLI_PATH, ".bin/sequelize")} --url=${this.databaseURL} db:create`,
                {env: process.env},
                (err) => {
                    if (err) {
                        if (String(err).indexOf("already exists") >= 0) {
                            return resolve();
                        } else {
                            return reject(new ClaireError("CANNOT_CREATE_DB", String(err)));
                        }
                    }
                    return resolve();
                }
            );
        });

        //-- connect and authenticate
        await new Promise<void>((resolve, reject) => {
            this.connection.authenticate()
                .then(() => {
                    return resolve();
                })
                .catch((err: any) => {
                    return reject(new ClaireError("DATABASE_CONNECTION_FAILED", err.stack || String(err)));
                });
        });

        //-- doing migration
        if (!!this.migrationDirectory) {
            this.logger.debug("Running migration...");
            await this.runMigration();
        }

        this.logger.debug("Generating in-memory schema");
        //-- inject modelConnection
        this.modelMetadata.forEach((metadata) => {
            this.schemas[metadata.modelName!].modelConnection = DefaultSqlAdapter.generateSchemaObject(this.connection, metadata);
        });

        this.logger.debug("Database adapter init succeeded");
    }

    public async stop(): Promise<void> {
        this.logger.debug("Disconnecting from database...");
        await this.connection.close();
        await super.stop();
    }

    private async runMigration(): Promise<void> {

        let prototypeFilePath = path.join(this.migrationDirectory!, "metadata.json");
        let migrationDir = path.join(this.migrationDirectory!, "scripts");
        if (!fs.existsSync(migrationDir)) {
            this.logger.debug("Migration directory not exists, creating empty folder");
            fs.mkdirSync(migrationDir, {recursive: true});
        }
        let fileExists = fs.existsSync(prototypeFilePath);
        if (!fileExists) {
            //-- write default empty array of metadata
            this.logger.debug("Migration metadata does not exist, creating one");
            fs.writeFileSync(prototypeFilePath, JSON.stringify([]));
        }

        //-- parse model metadata from json file
        this.logger.debug("Parsing migration metadata file");
        let oldMetadataJSON = JSON.parse(String(fs.readFileSync(prototypeFilePath)));
        let oldMetadata: ModelMetaData[] = [];
        oldMetadataJSON.forEach((jsonData: any) => {
            let modelMetaData = new ModelMetaData();
            modelMetaData.modelName = jsonData["modelName"];
            modelMetaData.tableName = jsonData["tableName"];
            modelMetaData.charset = jsonData["charset"];
            jsonData["fields"].forEach((field: any) => {
                let fieldMetaData = new FieldMetaData();
                fieldMetaData.fieldDataName = field["fieldDataName"];
                fieldMetaData.isPrimaryKey = field["isPrimaryKey"];
                fieldMetaData.isForeignKey = field["isForeignKey"];
                fieldMetaData.isAutoIncrement = field["isAutoIncrement"];
                fieldMetaData.isUnique = field["isUnique"];
                fieldMetaData.referModel = field["referModel"];
                fieldMetaData.dataType = field["dataType"];
                fieldMetaData.nullable = field["nullable"];
                fieldMetaData.defaultValue = field["defaultValue"];
                modelMetaData.addField(fieldMetaData);
            });
            oldMetadata.push(modelMetaData);
        });

        //-- calculate differences between two array of prototypes
        this.logger.debug("Calculating differences");
        let differences: ModelMetadataDifference[] = [];
        this.modelMetadata.forEach((metadata) => {
            let matchedMetadata = oldMetadata.find((m) => m.modelName === metadata.modelName);
            differences.push(DefaultSqlAdapter.getDifferences(matchedMetadata, metadata));
        });
        oldMetadata.forEach((metadata) => {
            let matchedMetadata = this.modelMetadata.find((m) => m.modelName === metadata.modelName);
            if (!matchedMetadata) {
                differences.push(DefaultSqlAdapter.getDifferences(metadata, undefined));
            }
        });

        //-- generating scripts from difference
        let scripts: { upTable: string[], upConstraint: string[], downTable: string[], downConstraint: string[] }[] = [];
        for (let i = 0; i < differences.length; i++) {
            let upTableQueries: string[] = [];
            let downTableQueries: string[] = [];
            let upConstraintQueries: string[] = [];
            let downConstraintQueries: string[] = [];

            let diff = differences[i];
            if (diff.removed && diff.removed.modelName) {
                let currentModelName = diff.removed.modelName;
                let currentModel = this.modelMetadata.find((meta) => meta.modelName === currentModelName);
                //-- check whether to remove entire table or just the column
                if (!diff.added) {
                    //-- remove entire table
                    upTableQueries.push(`queryInterface.dropTable('${currentModel && currentModel.tableName}',{}),\n`);
                } else {
                    //-- check to remove or update column
                    let oldFields = diff.removed.getFields();
                    let newFields = diff.added.getFields();
                    for (let j = 0; j < oldFields.length; j++) {
                        let oldField = oldFields[j];
                        let matchedField = newFields.find((f) => f.fieldDataName === oldField.fieldDataName);
                        if (!matchedField) {
                            //-- completely matched, remove field
                            upTableQueries.push(`queryInterface.removeColumn('${currentModel && currentModel.tableName}','${oldField.fieldDataName}',),\n`);
                        } else {
                            if (oldField.isUnique === true) {
                                //-- remove unique constraint
                                upConstraintQueries.push(`queryInterface.removeConstraint('${currentModel && currentModel.tableName}','${diff.removed}_${oldField.fieldDataName}_un'),\n`);
                                downConstraintQueries.push(`queryInterface.addConstraint('${currentModel && currentModel.tableName}',['${oldField.fieldDataName}'],{type:'unique',name:'${currentModel && currentModel.tableName}_${oldField.fieldDataName}_un'}),\n`);
                            }
                            if (oldField.isForeignKey === true) {
                                let referModel = this.modelMetadata.find((meta) => meta.modelName === oldField.referModel);
                                //-- remove foreign key constraint
                                upConstraintQueries.push(`queryInterface.removeConstraint('${currentModel && currentModel.tableName}','${currentModel && currentModel.tableName}_${oldField.fieldDataName}_fk'),\n`);
                                downConstraintQueries.push(`queryInterface.addConstraint('${currentModel && currentModel.tableName}',['${oldField.fieldDataName}'],{type:'foreign key',name:'${currentModel && currentModel.tableName}_${oldField.fieldDataName}_fk',references:{table:'${referModel && referModel.tableName}',field:'${referModel && referModel.getPrimaryKey().fieldDataName}'}}),\n`);
                            }
                        }
                    }
                }
            }
            if (diff.added && diff.added.modelName) {
                let currentModelName = diff.added.modelName;
                let currentModel = this.modelMetadata.find((meta) => meta.modelName === currentModelName);
                let fieldsQuery = "";
                let optionsQuery = "";
                if (!diff.removed) {
                    //-- create new table
                    fieldsQuery += "{\n";
                    let tableFields = diff.added.getFields();
                    for (let j = 0; j < tableFields.length; j++) {
                        let propertiesQuery = this.generatePropertiesQuery(tableFields[j]);
                        fieldsQuery += `${tableFields[j].fieldDataName}:${propertiesQuery},\n`;
                    }
                    fieldsQuery += "}\n";

                    //-- check options
                    optionsQuery += "{\n";
                    let charset = diff.added.charset;
                    switch (charset) {
                        case Charset.UTF8:
                            optionsQuery += `charset:'utf8',`;
                            break;
                        case Charset.ASCII:
                            optionsQuery += `charset:'ascii',`;
                            break;
                        default:
                            break;
                    }
                    optionsQuery += "\n}";
                    upTableQueries.push(`queryInterface.createTable('${currentModel && currentModel.tableName}',${fieldsQuery},${optionsQuery}),\n`);
                    downTableQueries.push(`queryInterface.dropTable('${currentModel && currentModel.tableName}',{}),\n`);

                    //-- check to add constraint
                    for (let j = 0; j < tableFields.length; j++) {
                        //-- check for unique constraint
                        if (tableFields[j].isUnique) {
                            upConstraintQueries.push(`queryInterface.addConstraint('${currentModel && currentModel.tableName}',['${tableFields[j].fieldDataName}'],{type:'unique',name:'${currentModel && currentModel.tableName}_${tableFields[j].fieldDataName}_un'}),\n`);
                            downConstraintQueries.push(`queryInterface.removeConstraint('${currentModel && currentModel.tableName}','${currentModel && currentModel.tableName}_${tableFields[j].fieldDataName}_un'),\n`);
                        }
                        if (tableFields[j].isForeignKey) {
                            let referModel = this.modelMetadata.find((m) => m.modelName === tableFields[j].referModel);
                            upConstraintQueries.push(`queryInterface.addConstraint('${currentModel && currentModel.tableName}',['${tableFields[j].fieldDataName}'],{type:'foreign key',name:'${currentModel && currentModel.tableName}_${tableFields[j].fieldDataName}_fk',references:{table:'${referModel && referModel.tableName}',field:'${referModel && referModel.getPrimaryKey().fieldDataName}'}}),\n`);
                            downConstraintQueries.push(`queryInterface.removeConstraint('${currentModel && currentModel.tableName}','${currentModel && currentModel.tableName}_${tableFields[j].fieldDataName}_fk'),\n`);
                        }
                    }
                } else {
                    //-- check to add or update column
                    let tableFields = diff.added.getFields();
                    fieldsQuery += "{\n";
                    for (let j = 0; j < tableFields.length; j++) {
                        let field = tableFields[j];
                        let propertiesQuery = this.generatePropertiesQuery(field);
                        let oldMatchedField = diff.removed.getFieldByDataName(field.fieldDataName || "");
                        if (!oldMatchedField) {
                            //-- add field
                            upTableQueries.push(`queryInterface.addColumn('${currentModel && currentModel.tableName}','${field.fieldDataName}',${propertiesQuery}),\n`);
                            downTableQueries.push(`queryInterface.removeColumn('${currentModel && currentModel.tableName}','${field.fieldDataName}',),\n`);
                        } else {
                            if (field.isUnique === true) {
                                //-- add unique constraint
                                upConstraintQueries.push(`queryInterface.addConstraint('${currentModel && currentModel.tableName}',['${field.fieldDataName}'],{type:'unique',name:'${currentModel && currentModel.tableName}_${field.fieldDataName}_un'}),\n`);
                                downConstraintQueries.push(`queryInterface.removeConstraint('${currentModel && currentModel.tableName}','${currentModel && currentModel.tableName}_${field.fieldDataName}_un'),\n`);
                            }
                            if (field.isForeignKey === true) {
                                //-- add foreign key constraint
                                let referModel = this.modelMetadata.find((meta) => meta.modelName === field.referModel);
                                upConstraintQueries.push(`queryInterface.addConstraint('${currentModel && currentModel.tableName}',['${field.fieldDataName}'],{type:'foreign key',name:'${currentModel && currentModel.tableName}_${field.fieldDataName}_fk',references:{table:'${referModel && referModel.tableName}',field:'${referModel && referModel.getPrimaryKey().fieldDataName}'}}),\n`);
                                downConstraintQueries.push(`queryInterface.removeConstraint('${currentModel && currentModel.tableName}','${currentModel && currentModel.tableName}_${field.fieldDataName}_fk'),\n`);
                            }
                            if (field.dataType !== undefined || field.nullable !== undefined) {
                                let currentField = currentModel!.getFieldByDataName(field.fieldDataName!);
                                let oldField = oldMetadata.find((m) => m.modelName === currentModel!.modelName)!.getFieldByDataName(field.fieldDataName!);
                                upTableQueries.push(`queryInterface.changeColumn('${currentModel && currentModel.tableName}','${field.fieldDataName}',${this.generatePropertiesQuery(Object.assign({}, currentField, field))}),\n`);
                                downTableQueries.push(`queryInterface.changeColumn('${currentModel && currentModel.tableName}','${field.fieldDataName}',${this.generatePropertiesQuery(oldField!)}),\n`);
                            }
                        }
                    }
                    fieldsQuery += "\n}\n";
                }
            }

            if (upTableQueries.length || downTableQueries.length || upConstraintQueries.length || downConstraintQueries.length) {
                scripts.push({
                    upTable: upTableQueries,
                    downTable: downTableQueries,
                    upConstraint: upConstraintQueries,
                    downConstraint: downConstraintQueries
                });
            }
        }

        if (!scripts.length) {
            //-- exec the migration
            this.logger.debug("No difference, running migration");
            await new Promise<void>((resolve, reject) => {
                exec(
                    `${path.join(__dirname, DefaultSqlAdapter.SEQUELIZE_CLI_PATH, ".bin/sequelize")} --url=${this.databaseURL} --migrations-path=${migrationDir} db:migrate`,
                    {env: process.env},
                    (err: any) => {
                        if (err) {
                            return reject(new ClaireError("MIGRATION_FAILED", err.stack || String(err)));
                        }
                        return resolve();
                    }
                );
            });
            return;
        } else {
            //-- generate migration file
            let migrationFileNames: string[] = [];
            let migrationFunction = async (scripts: { up: string[], down: string[] }, index: number, migrationType: string) => {
                if (!scripts.up.length && !scripts.down.length)
                    return;

                let generationResult = await new Promise<string | ClaireError>((resolve) => {
                    exec(
                        `${path.join(__dirname, DefaultSqlAdapter.SEQUELIZE_CLI_PATH, ".bin/sequelize")} --migrations-path=${migrationDir} migration:generate --name auto-migration`,
                        {},
                        (err, stdout) => {
                            if (err) {
                                return resolve(new ClaireError("INTERNAL_SYSTEM_ERROR", err.stack || String(err)));
                            }
                            return resolve(stdout);
                        }
                    );
                });
                if (generationResult instanceof ClaireError) {
                    return generationResult;
                }

                //-- use regex to capture generated file name
                let regex = new RegExp(`[0-9]*-auto-migration.js`);
                let fileName = regex.exec(generationResult);
                if (fileName === null) {
                    return new ClaireError("MIGRATION_GENERATION_FAILED", generationResult);
                }

                //-- replace content of generated migration file
                let migrationFilePath = path.join(migrationDir, fileName[0]);

                //-- wait until the system populate the new file
                this.logger.debug("Waiting for system to create migration file: ", migrationFilePath);
                while (!fs.existsSync(migrationFilePath)) {
                    await new Promise((resolve) => {
                        setTimeout(resolve, 1000);
                    });
                }

                let fileContent = String(fs.readFileSync(migrationFilePath));
                let commentRegex = new RegExp(DefaultSqlAdapter.COMMENT_BLOCK_REGEX_PATTERN);
                let upScript = "return [\n" + scripts.up.reduce((accumulator, e) => accumulator + e, "") + "]" + DefaultSqlAdapter.SERIAL_PROMISE;
                let downScript = "return [\n" + scripts.down.reverse().reduce((accumulator, e) => accumulator + e, "") + "]" + DefaultSqlAdapter.SERIAL_PROMISE;
                fileContent = fileContent.replace(commentRegex, upScript);
                fileContent = fileContent.replace(commentRegex, downScript);
                this.logger.debug("writing to file", migrationFilePath);
                fs.writeFileSync(migrationFilePath, fileContent);

                //-- rename, use i to avoid file overlapping
                let newName = fileName[0].substr(0, fileName[0].length - 3) + `-${index}-${migrationType}.js`;
                this.logger.debug("renaming file", migrationFilePath);
                fs.renameSync(migrationFilePath, path.join(migrationDir, newName));
                migrationFileNames.push(newName);
                return;
            };

            for (let i = 0; i < scripts.length; i++) {
                await migrationFunction({
                    up: scripts[i].upTable,
                    down: scripts[i].downTable
                }, i, "0");
            }

            for (let i = 0; i < scripts.length; i++) {
                await migrationFunction({
                    up: scripts[i].upConstraint,
                    down: scripts[i].downConstraint
                }, i, "1");
            }

            //-- update metadata file
            this.logger.debug("updating metadata file");
            fs.writeFileSync(prototypeFilePath, JSON.stringify(this.modelMetadata));
            throw new ClaireError("MIGRATION_REVIEW_NEEDED", JSON.stringify(migrationFileNames));
        }
    }

    private generatePropertiesQuery(fieldProperties: FieldMetaData): string {

        let propertiesQuery = "{";

        //-- check field properties
        if (fieldProperties.isPrimaryKey === true) {
            propertiesQuery += `primaryKey:true,`;
        }

        if (fieldProperties.isForeignKey === true) {
            //-- get table name of the refer model
            let referModel = this.modelMetadata.find((meta) => meta.modelName === fieldProperties.referModel);
            propertiesQuery += `references:{model:'${referModel && referModel.tableName}',key:'${referModel && referModel.getPrimaryKey().fieldDataName}'},`;
        }

        if (fieldProperties.isAutoIncrement === true) {
            propertiesQuery += `autoIncrement:true,`;
        }

        propertiesQuery += `allowNull:${fieldProperties.nullable ? "true" : "false"},`;

        if (fieldProperties.defaultValue !== undefined) {
            propertiesQuery += `defaultValue:${fieldProperties.defaultValue},`;
        }

        if (fieldProperties.dataType !== undefined) {
            switch (fieldProperties.dataType) {
                case DataType.STRING:
                    propertiesQuery += `type:Sequelize.STRING,`;
                    break;
                case DataType.TEXT:
                    propertiesQuery += `type:Sequelize.TEXT,`;
                    break;
                case DataType.INTEGER:
                    propertiesQuery += `type:Sequelize.INTEGER,`;
                    break;
                case DataType.BOOL:
                    propertiesQuery += `type:Sequelize.BOOLEAN,`;
                    break;
            }
        }
        propertiesQuery += "}";

        return propertiesQuery;
    }

    private static getDifferences(oldMeta: ModelMetaData | undefined, newMeta: ModelMetaData | undefined): ModelMetadataDifference {
        let result: ModelMetadataDifference = {
            removed: undefined,
            added: undefined,
        };

        if (oldMeta === undefined) {
            result.added = newMeta;
        } else if (newMeta === undefined) {
            result.removed = oldMeta;
        } else {
            let removed = new ModelMetaData();
            let added = new ModelMetaData();

            //-- this value must be present
            removed.modelName = oldMeta.modelName;
            added.modelName = newMeta.modelName;

            //-- check tableName
            if (oldMeta.tableName !== newMeta.tableName) {
                removed.tableName = oldMeta.tableName;
                added.tableName = newMeta.tableName;
            }

            //-- check charset
            if (oldMeta.charset !== newMeta.charset) {
                removed.charset = oldMeta.charset;
                added.charset = newMeta.charset;
            }

            //-- check fields
            let oldMetaFields = oldMeta.getFields();
            for (let i = 0; i < oldMetaFields.length; i++) {
                let oldField = oldMetaFields[i];
                if (oldField.fieldDataName) {
                    let matchedField = newMeta.getFieldByDataName(oldField.fieldDataName);
                    if (!matchedField) {
                        removed.addField(oldField);
                    } else {
                        let removedField = new FieldMetaData();
                        let addedField = new FieldMetaData();
                        removedField.fieldDataName = oldField.fieldDataName;
                        addedField.fieldDataName = matchedField.fieldDataName;
                        //-- comparing properties
                        if (oldField.isPrimaryKey !== matchedField.isPrimaryKey) {
                            removedField.isPrimaryKey = oldField.isPrimaryKey;
                            addedField.isPrimaryKey = matchedField.isPrimaryKey;
                        }
                        if (oldField.isForeignKey !== matchedField.isForeignKey) {
                            removedField.isForeignKey = oldField.isForeignKey;
                            addedField.isForeignKey = matchedField.isForeignKey;
                        }
                        if (oldField.isAutoIncrement !== matchedField.isAutoIncrement) {
                            removedField.isAutoIncrement = oldField.isAutoIncrement;
                            addedField.isAutoIncrement = matchedField.isAutoIncrement;
                        }
                        if (oldField.isUnique !== matchedField.isUnique) {
                            removedField.isUnique = oldField.isUnique;
                            addedField.isUnique = matchedField.isUnique;
                        }
                        if (oldField.referModel !== matchedField.referModel) {
                            removedField.referModel = oldField.referModel;
                            addedField.referModel = matchedField.referModel;
                        }
                        if (oldField.dataType !== matchedField.dataType) {
                            removedField.dataType = oldField.dataType;
                            addedField.dataType = matchedField.dataType;
                        }
                        if (oldField.nullable !== matchedField.nullable) {
                            removedField.nullable = oldField.nullable;
                            addedField.nullable = matchedField.nullable;
                        }
                        if (oldField.defaultValue !== matchedField.defaultValue) {
                            removedField.defaultValue = oldField.defaultValue;
                            addedField.defaultValue = matchedField.defaultValue;
                        }
                        removed.addField(removedField);
                        added.addField(addedField);
                    }
                }
            }

            let newMetaFields = newMeta.getFields();
            for (let i = 0; i < newMetaFields.length; i++) {
                let newField = newMetaFields[i];
                if (newField.fieldDataName) {
                    let matchedField = oldMeta.getFieldByDataName(newField.fieldDataName);
                    if (!matchedField) {
                        added.addField(newField);
                    }
                }
            }

            result.removed = ModelMetaData.isNotEmptyMetadata(removed) ? removed : undefined;
            result.added = ModelMetaData.isNotEmptyMetadata(added) ? added : undefined;
        }

        return result;
    }

    private static generateSchemaObject(builder: Sequelize, tablePrototype: ModelMetaData): any {
        let schema;
        let schemaDefinition: any = {};

        if (tablePrototype.tableName) {
            tablePrototype.getFields().forEach((field) => {
                if (field.fieldDataName) {
                    schemaDefinition[field.fieldDataName] = {};
                    //-- check type
                    switch (field.dataType) {
                        case DataType.INTEGER:
                            schemaDefinition[field.fieldDataName]["type"] = INTEGER;
                            break;
                        case DataType.TEXT:
                            schemaDefinition[field.fieldDataName]["type"] = TEXT;
                            break;
                        case DataType.STRING:
                            schemaDefinition[field.fieldDataName]["type"] = STRING;
                            break;
                        case DataType.BOOL:
                            schemaDefinition[field.fieldDataName]["type"] = BOOLEAN;
                            break;
                    }

                    //-- check default value
                    if (!Utils.isNullOrUndefined(field.defaultValue)) {
                        schemaDefinition[field.fieldDataName]["defaultValue"] = field.defaultValue;
                    }

                    //-- check primaryKey
                    if (field.isPrimaryKey === true) {
                        schemaDefinition[field.fieldDataName]["primaryKey"] = true;
                    }

                    //-- check auto increment
                    if (field.isAutoIncrement === true) {
                        schemaDefinition[field.fieldDataName]["autoIncrement"] = true;
                    }

                    //-- check nullable
                    schemaDefinition[field.fieldDataName]["allowNull"] = field.nullable;
                }
            });
            schema = builder.define(tablePrototype.tableName, schemaDefinition, {
                tableName: tablePrototype.tableName
            });
        }
        return schema;
    }

    public async createTransaction(): Promise<ITransaction> {
        let transaction = await this.connection.transaction();
        return new TransactionAdapter(transaction, this.connection, this.modelMetadata, this.schemas);
    }

}
