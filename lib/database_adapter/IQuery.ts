import {QueryOperator} from "./QueryOperator";

/**
 * Subset of the keys of an interface/class.
 */
export type SubKeySet<T> = keyof Partial<T>;

/**
 * Query condition based on a interface/class.
 */
export type QueryCondition<T> = { [name in SubKeySet<T>]?: QueryOperator };


export interface IQuery<T> {

    saveOne(modelInstance: Partial<T>): Promise<T>;

    saveMany(modelInstances: Partial<T>[]): Promise<T[]>;

    updateOne(modelInstance: T): Promise<T>;

    updateMany(queries: QueryCondition<T>[], update: Partial<T>): Promise<number>;

    deleteOne(modelInstance: T): Promise<T>;

    deleteMany(queries: QueryCondition<T>[]): Promise<number>;

    getOne(queries?: QueryCondition<T>[], options?: { projection?: SubKeySet<T>[] }): Promise<T | undefined>;

    getMany(queries?: QueryCondition<T>[], options?: { projection?: SubKeySet<T>[], limit?: number, page?: number }): Promise<T[]>;

    rawQuery(query: any): Promise<any>;

}
