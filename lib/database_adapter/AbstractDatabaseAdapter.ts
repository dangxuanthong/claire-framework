import {AbstractModel, ClaireError, IAppContext, Initable, ITransaction} from "..";
import {MODEL_PROTOTYPE_METADATA_FIELD} from "../model/TableMapper";
import {ModelMetaData} from "../model/ModelMetaData";
import {Errors} from "../system/Errors";
import {IQuery} from "./IQuery";
import {IDatabaseAdapter} from "./IDatabaseAdapter";

export enum SqlProvider {
    MY_SQL = "mysql",
    POSTGRES = "postgres",
}


export enum NoSqlProvider {
    MONGODB = "mongodb"
}

export abstract class AbstractDatabaseAdapter implements IDatabaseAdapter, Initable {

    public models: typeof AbstractModel[];
    public modelMetadata: ModelMetaData[];
    protected schemas: { [name: string]: { modelConnection: any } };

    protected constructor(models: { new(...args: any[]): AbstractModel }[]) {
        //-- check for validity of models
        this.models = [];
        this.modelMetadata = [];
        for (let i = 0; i < models.length; i++) {
            let model = models[i];
            let metaData = model.prototype[MODEL_PROTOTYPE_METADATA_FIELD] as ModelMetaData;
            if (!metaData || !metaData.assertValid) {
                throw new ClaireError(Errors.database.INVALID_MODEL_METADATA, model.name);
            }
            metaData.assertValid();
            this.models.push(model);
            this.modelMetadata.push(metaData);
        }

        this.modelMetadata = ModelMetaData.dependencySort(this.modelMetadata);
        this.schemas = {};
        this.models.forEach((model) => {
            this.schemas[model.name] = {modelConnection: undefined};
        });
    }

    public async init(appContext: IAppContext): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract use<T extends AbstractModel>(model: { new(...args: any[]): T }): IQuery<T>;

    public abstract createTransaction(): Promise<ITransaction>;

}
