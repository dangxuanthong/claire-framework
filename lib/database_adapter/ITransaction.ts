import {IQueryProvider} from "./IQueryProvider";

export interface ITransaction extends IQueryProvider {

    commit(): Promise<void>;

    rollback(): Promise<void>;

}
