import {IQuery} from "./IQuery";
import {AbstractModel} from "..";

export interface IQueryProvider {

    /**
     * Use a model to provide the query interface for this model.
     * @param model the model to get query interface
     */
    use<T extends AbstractModel>(model: new(...args: any[]) => T): IQuery<T>;

}
