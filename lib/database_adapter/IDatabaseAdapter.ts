import {ITransaction} from "./ITransaction";
import {IQueryProvider} from "./IQueryProvider";

export interface IDatabaseAdapter extends IQueryProvider {

    /**
     * Create a transaction object.
     */
    createTransaction(): Promise<ITransaction>;

}
