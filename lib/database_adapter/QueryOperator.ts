export enum Operator {
    COMMON_EQUALITY,
    COMMON_INEQUALITY,
    COMMON_BELONG,

    STRING_CONTAIN,
    STRING_REGEX,

    NUMBER_GT,
    NUMBER_GTE,
    NUMBER_LT,
    NUMBER_LTE,
}

export type QueryOperator = { operator: Operator, value: any };

//-- common operators
export const eq = (value?: string | number | boolean): QueryOperator => {
    return {operator: Operator.COMMON_EQUALITY, value};
};
export const neq = (value?: string | number | boolean): QueryOperator => {
    return {operator: Operator.COMMON_INEQUALITY, value};
};
export const belongs = <T>(values?: T[]): QueryOperator => {
    return {operator: Operator.COMMON_BELONG, value: values};
};

//-- string operators
export const contains = (value?: string): QueryOperator => {
    return {operator: Operator.STRING_CONTAIN, value};
};

export const regex = (value?: string): QueryOperator => {
    return {operator: Operator.STRING_REGEX, value};
};

//-- number operators
export const gt = (value?: number): QueryOperator => {
    return {operator: Operator.NUMBER_GT, value};
};

export const gte = (value?: number): QueryOperator => {
    return {operator: Operator.NUMBER_GTE, value};
};

export const lt = (value?: number): QueryOperator => {
    return {operator: Operator.NUMBER_LT, value};
};

export const lte = (value?: number): QueryOperator => {
    return {operator: Operator.NUMBER_LTE, value};
};
