import {IQuery, QueryCondition, SubKeySet} from "./IQuery";
import {AbstractModel} from "..";
import {ModelMetaData} from "../model/ModelMetaData";

export abstract class AbstractQuery<T> implements IQuery<T> {

    public abstract deleteMany(queries: QueryCondition<T>[]): Promise<number>;

    public abstract deleteOne(modelInstance: Partial<T>): Promise<T>;

    public abstract getMany(queries?: QueryCondition<T>[], options?: { projection?: SubKeySet<T>[]; limit?: number; page?: number }): Promise<T[]>;

    public abstract getOne(queries?: QueryCondition<T>[], options?: { projection?: SubKeySet<T>[] }): Promise<T | undefined>;

    public abstract rawQuery(query: any): Promise<any>;

    public abstract saveMany(modelInstances: Partial<T>[]): Promise<T[]>;

    public abstract saveOne(modelInstance: Partial<T>): Promise<T>;

    public abstract updateMany(queries: QueryCondition<T>[], update: Partial<T>): Promise<number>;

    public abstract updateOne(modelInstance: Partial<T>): Promise<T>;

    protected abstract getQueryObjectFromQueryConditions(queries: QueryCondition<T>[]): any;

    //-- properties
    protected readonly currentModel: new (...args: any[]) => AbstractModel;
    protected readonly currentMetadata: ModelMetaData;
    protected readonly currentSchema: { modelConnection: any };
    protected readonly defaultLogicValues: Partial<T>;

    protected constructor(model: new (...args: any[]) => AbstractModel, metadata: ModelMetaData, schema: { modelConnection: any }) {
        this.currentModel = model;
        this.currentMetadata = metadata;
        this.currentSchema = schema;
        this.defaultLogicValues = this.currentMetadata.getDefaultLogicValues();
    }

    protected getProjectionFields(projection: SubKeySet<T>[]): string[] {
        return projection.map((key: any) => this.currentMetadata.getFieldByLogicName(key)!.fieldDataName!);
    }

    protected convertToLogicObjects(dataObjects: any[], projection?: SubKeySet<T>[]): T[] {
        return dataObjects.map(dataObject => {
            let result = new this.currentModel() as any;
            // @ts-ignore
            this.currentMetadata.getFields().filter((field) => !projection || projection.includes(field.fieldDataName!)).forEach((field) => {
                if (dataObject[field.fieldDataName!] !== null && dataObject[field.fieldDataName!] !== undefined) {
                    result[field.fieldLogicName!] = dataObject[field.fieldDataName!];
                }
            });
            return result as T;
        });
    }

    protected convertToDataObjects(modelInstances: Partial<T>[]): any {
        return modelInstances.map(instance => {
            let result: any = {};
            this.currentMetadata.getFields().forEach(field => {
                // @ts-ignore
                result[field.fieldDataName!] = instance[field.fieldDataName!] || instance[field.fieldLogicName];
            });
            return result;
        });
    }

}
