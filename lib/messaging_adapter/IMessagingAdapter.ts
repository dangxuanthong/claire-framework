export type MessageCallback = (data: string) => void;

export enum ChannelType {
    UNICAST,
    MULTICAST,
}


export interface IMessagingAdapter {

    isChannelExisted(channelName: string): Promise<boolean>;

    createChannel(channelName: string, channelType: ChannelType): Promise<boolean>;

    removeChannel(channelName: string): Promise<boolean>;

    publish(channelName: string, data: string): Promise<void>;

    subscribe(channelName: string, callback: MessageCallback): number;

    unsubscribe(subscriberId: number): void;

    send(channelName: string, data: string): Promise<void>;

    receive(channelName: string, timeoutMs: number): Promise<string>;

}