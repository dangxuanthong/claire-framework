import {ChannelType, IMessagingAdapter, MessageCallback} from "./IMessagingAdapter";
import {Initable} from "..";

export abstract class AbstractMessagingAdapter implements IMessagingAdapter, Initable {

    public async init(): Promise<void> {
        return;
    }

    public async stop(): Promise<void> {
        return;
    }

    public abstract isChannelExisted(channelName: string): Promise<boolean>;

    public abstract createChannel(channelName: string, channelType: ChannelType): Promise<boolean>;

    public abstract removeChannel(channelName: string): Promise<boolean>;

    public abstract publish(channelName: string, data: string): Promise<void>;

    public abstract subscribe(channelName: string, callback: MessageCallback): number;

    public abstract unsubscribe(subscriberId: number): void;

    public abstract send(channelName: string, data: string): Promise<void>;

    public abstract receive(channelName: string, timeoutMs: number): Promise<string>;

}
