import redis from "redis";
import {AbstractMessagingAdapter} from "./AbstractMessagingAdapter";
import {ChannelType, MessageCallback} from "./IMessagingAdapter";
import {ClaireError} from "..";
import {Errors} from "../system/Errors";

interface Listener {
    listenerId: number;
    channelName: string;
    callback: MessageCallback;
}


export class DefaultRedisMessagingAdapter extends AbstractMessagingAdapter {

    private static listenerCounter: number = 0;

    private readonly redisConnectionString: string;
    private listeners: Listener[];
    private redisPubClient: any;
    private redisSubClient: any;

    public constructor(redisConnectionString: string) {
        super();
        this.redisConnectionString = redisConnectionString;
        this.listeners = [];

    }

    public async init(): Promise<void> {
        await super.init();
        //-- using Redis
        this.redisPubClient = redis.createClient(this.redisConnectionString);
        this.redisSubClient = redis.createClient(this.redisConnectionString);

        let initResult = await new Promise<true | ClaireError>((resolve) => {

            Promise.all([this.redisPubClient, this.redisSubClient].map((client) => {
                return new Promise((resolve, reject) => {
                    client.on("connect", (err: any) => {
                        if (err) {
                            return reject(new ClaireError(Errors.system.CONNECTION_ERROR, err.stack || String(err)));
                        }
                        return resolve(true);
                    });
                });
            }))
                .then(() => {
                    resolve(true);
                })
                .catch((err) => {
                    if (err instanceof ClaireError) {
                        resolve(err);
                    } else {
                        resolve(new ClaireError(Errors.system.INTERNAL_SYSTEM_ERROR, err.stack || String(err)));
                    }
                })
        });

        if (initResult instanceof ClaireError) {
            throw initResult;
        }

        this.redisSubClient.on("message", (channel: string, data: string) => {
            this.listeners.forEach((listener) => {
                if (listener.channelName === channel) {
                    listener.callback(data);
                }
            });
        });

    }

    public async stop(): Promise<void> {
        this.redisPubClient.quit();
        this.redisSubClient.quit();
        await super.stop();
        return;
    }

    public async createChannel(channelName: string, channelType: ChannelType): Promise<boolean> {
        return true;
    }

    public async isChannelExisted(channelName: string): Promise<boolean> {
        return true;
    }

    public async publish(channelName: string, data: string): Promise<void> {
        this.redisPubClient.publish(channelName, data);
    }

    public receive(channelName: string, timeoutMs: number): Promise<string> {
        throw new ClaireError(Errors.system.NOT_IMPLEMENTED);
    }

    public removeChannel(channelName: string): Promise<boolean> {
        throw new ClaireError(Errors.system.NOT_IMPLEMENTED);
    }

    public subscribe(channelName: string, callback: MessageCallback): number {
        let listenerId = DefaultRedisMessagingAdapter.listenerCounter++;
        this.listeners.push({
            listenerId,
            channelName,
            callback,
        });

        //-- subscribe to redis
        this.redisSubClient.subscribe(channelName);

        return listenerId;
    }

    public send(channelName: string, data: string): Promise<void> {
        throw new ClaireError(Errors.system.NOT_IMPLEMENTED);
    }


    public unsubscribe(subscriberId: number): void {
        let index = this.listeners.findIndex((listener) => listener.listenerId === subscriberId);
        if (index >= 0) {
            this.listeners = [
                ...this.listeners.slice(0, index),
                ...this.listeners.slice(index + 1),
            ];
        }
    }

}
