import {ILogger, LogLimit} from "./ILogger";

export abstract class AbstractLogger implements ILogger {

    protected logLimit: LogLimit;

    protected constructor(logLimit: LogLimit) {
        this.logLimit = logLimit;
    }

    public abstract error(...logContent: any[]): void;

    public abstract warn(...logContent: any[]): void;

    public abstract log(...logContent: any[]): void;
    
    public abstract debug(...logContent: any[]): void;

}
