export enum LogLimit {
    ERROR = 1,
    WARN = 2,
    INFO = 3,
    DEBUG = 4,
}

export interface ILogger {

    error(...logContent: any[]): void;

    warn(...logContent: any[]): void;

    log(...logContent: any[]): void;

    debug(...logContent: any[]): void;

}
