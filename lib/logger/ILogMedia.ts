import {LogLimit} from "./ILogger";

export interface ILogMedia {

    write(content: string, logLevel: LogLimit): void;

}