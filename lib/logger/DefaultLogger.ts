import moment from "moment";
import {LogLimit} from "./ILogger";
import {AbstractLogger} from "./AbstractLogger";
import {AbstractLogMedia} from "./AbstractLogMedia";
import {Utils} from "../system/Utils";

// credit: https://stackoverflow.com/questions/11616630/how-can-i-print-a-circular-structure-in-a-json-like-format/11616993#11616993
const JSONStringify = (obj: any, space: number = 4) => {
    let cache: any[] = [];
    return JSON.stringify(obj,
        //-- custom replacer fxn - gets around "TypeError: Converting circular structure to JSON"
        function (key, value) {
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    //-- circular reference found, discard key
                    return;
                }
                //-- store value in our collection
                cache.push(value);
            }
            return value;
        }, space);
};


export class DefaultLogger extends AbstractLogger {

    private readonly timeFormat?: string;
    private readonly timeDistanceMs: number;
    private readonly logMedia: AbstractLogMedia[];
    private lastLogTime: number;
    private lastLogLimit: LogLimit;

    constructor(logLimit: LogLimit, logMedia: (AbstractLogMedia | undefined)[], timeFormat?: string, timeDistanceMs?: number) {
        super(logLimit);
        this.lastLogLimit = logLimit;
        this.logMedia = Utils.getCleanArray(logMedia);
        this.timeFormat = timeFormat;
        this.timeDistanceMs = timeDistanceMs || 0;
        this.lastLogTime = Date.now() - this.timeDistanceMs;
    }

    private writeToMedia(content: string, logLevel: LogLimit): void {
        for (let i = 0; i < this.logMedia.length; i++) {
            this.logMedia[i].write(content, logLevel);
        }
    }

    private prepareContent(logContent: any[], logLimit: LogLimit): string {
        let content = "";
        let timestamp = Date.now();
        if ((timestamp - this.lastLogTime >= this.timeDistanceMs) || (this.lastLogLimit !== logLimit)) {
            this.lastLogTime = timestamp;
            let time = (this.timeFormat) ? moment(timestamp).format(this.timeFormat) : timestamp.toString();
            content = content + `\n\n[${time}]`;
        }
        content = content + "\n> ";
        logContent.forEach((c) => {
            content = content + " " + JSONStringify(c, 4);
        });
        return content;
    }

    public error(...logContent: any[]): void {
        if (this.logLimit >= LogLimit.ERROR) {
            this.writeToMedia(this.prepareContent(logContent, LogLimit.ERROR), LogLimit.ERROR);
            this.lastLogLimit = LogLimit.ERROR;
        }
    }

    public warn(...logContent: any[]): void {
        if (this.logLimit >= LogLimit.WARN) {
            this.writeToMedia(this.prepareContent(logContent, LogLimit.WARN), LogLimit.WARN);
            this.lastLogLimit = LogLimit.WARN;
        }
    }

    public log(...logContent: any[]): void {
        if (this.logLimit >= LogLimit.INFO) {
            this.writeToMedia(this.prepareContent(logContent, LogLimit.INFO), LogLimit.INFO);
            this.lastLogLimit = LogLimit.INFO;
        }
    }

    public debug(...logContent: any[]): void {
        if (this.logLimit >= LogLimit.DEBUG) {
            this.writeToMedia(this.prepareContent(logContent, LogLimit.DEBUG), LogLimit.DEBUG);
            this.lastLogLimit = LogLimit.DEBUG;
        }
    }

}
