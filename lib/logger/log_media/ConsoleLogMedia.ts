import colors from "colors/safe";
import {AbstractLogMedia} from "../AbstractLogMedia";
import {LogLimit} from "../ILogger";

export class ConsoleLogMedia extends AbstractLogMedia {

    private readonly withColor: boolean;

    public constructor(withColor?: boolean) {
        super();
        this.withColor = withColor !== undefined ? withColor : true;
    }

    public write(content: string, logLevel: LogLimit): void {
        if (this.withColor) {
            switch (logLevel) {
                case LogLimit.ERROR:
                    content = colors.red(content);
                    break;
                case LogLimit.WARN:
                    content = colors.yellow(content);
                    break;
                case LogLimit.INFO:
                    content = colors.white(content);
                    break;
                case LogLimit.DEBUG:
                    content = colors.green(content);
            }
        }
        process.stdout.write(content);
    }

}
