import fs from "fs";
import path from "path";
import {AbstractLogMedia} from "../AbstractLogMedia";
import {ClaireError} from "../..";
import {Errors} from "../../system/Errors";
import {LogLimit} from "../ILogger";

export class FileLogMedia extends AbstractLogMedia {


    /**
     * The directory to contain all log files.
     */
    private readonly logDirectory: string;

    /**
     * Number of date for log rotation. If the value is zero or negative, rotation is disabled and all logs will be recorded.
     * If the value is positive, then the log content will be rotated and kept for rotationDayInterval days. Older logs
     * will be overwritten.
     */
    private readonly rotationDayInterval: number;

    /**
     * Whether to separate the log content into files corresponding to different levels.
     */
    private readonly separateLevel: boolean;

    /**
     * Contains the current log day to check for log rotation.
     */
    private currentLogDay: number;

    public constructor(logDirectory: string, rotationDayInterval: number, separateLevel: boolean = false) {
        super();
        if (!fs.existsSync(logDirectory)) {
            fs.mkdirSync(logDirectory, {recursive: true});
        }
        this.logDirectory = logDirectory;
        //-- +1 to prevent the case when rotationDayInterval=1 will never remove itself.
        this.rotationDayInterval = rotationDayInterval + 1;
        this.separateLevel = separateLevel;

        if (this.rotationDayInterval > 0) {
            this.currentLogDay = FileLogMedia.getCurrentLogDay(this.rotationDayInterval);
        }
    }

    public write(content: string, logLevel: LogLimit): void {
        try {
            if (this.rotationDayInterval > 0) {
                let today = FileLogMedia.getCurrentLogDay(this.rotationDayInterval);
                if (today !== this.currentLogDay) {
                    //-- day switch, remove old log file if existed
                    let tobeRemoved = (today + 1) % this.rotationDayInterval;
                    if (this.separateLevel) {
                        Object.values(LogLimit).forEach((level) => {
                            this.removeOldLogFile(FileLogMedia.getLogFileName(true, level as LogLimit, tobeRemoved));
                        });
                    } else {
                        this.removeOldLogFile(FileLogMedia.getLogFileName(false, undefined, tobeRemoved));
                    }
                    //-- re-assign
                    this.currentLogDay = today;
                }
            }

            fs.writeFileSync(path.join(this.logDirectory, FileLogMedia.getLogFileName(this.separateLevel, logLevel, this.rotationDayInterval)), content, {flag: "a"});

        } catch (err) {
            throw new ClaireError(Errors.system.FILE_ACCESS_ERROR, err.stack || String(err));
        }
    }

    private removeOldLogFile(fileName: string): void {
        try {
            fs.unlinkSync(path.join(this.logDirectory, fileName));
        } catch (err) {
            //-- file access error or file not found, ignore
        }
    }

    private static getLogFileName(separateLevel: boolean, logLevel: LogLimit | undefined, rotationDayInterval: number): string {
        let fileName = "claire";
        if (rotationDayInterval > 0) {
            fileName += `-${FileLogMedia.getCurrentLogDay(rotationDayInterval)}`;
        }
        if (separateLevel) {
            switch (logLevel) {
                case LogLimit.ERROR:
                    fileName += "-error";
                    break;
                case LogLimit.WARN:
                    fileName += "-warn";
                    break;
                case LogLimit.INFO:
                    fileName += "-info";
                    break;
                case LogLimit.DEBUG:
                    fileName += "-debug";
                    break;
                default:
                    break;
            }
        }
        return fileName + ".log";
    }

    private static getCurrentLogDay(dayInterval: number): number {
        return Math.trunc(Date.now() / 86400000) % dayInterval;
    }

}
