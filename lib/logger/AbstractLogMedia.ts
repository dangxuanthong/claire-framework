import {ILogMedia} from "./ILogMedia";
import {LogLimit} from "./ILogger";

export abstract class AbstractLogMedia implements ILogMedia {

    protected constructor() {

    }

    public abstract write(content: string, logLevel: LogLimit): void;

}