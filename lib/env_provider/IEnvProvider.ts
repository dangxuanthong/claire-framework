export interface IEnvProvider {

    getCurrentEnv(): string;

    load<T>(template: new(...args: any[]) => T): T;

}
