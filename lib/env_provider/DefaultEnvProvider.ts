import path = require("path");

import {ClaireError} from "..";
import {AbstractEnvProvider} from "./AbstractEnvProvider";
import {Errors} from "../system/Errors";
import {ENV_VAR_PROTOTYPE_KEY} from "./EnvMapper";

export class DefaultEnvProvider extends AbstractEnvProvider {

    private readonly currentEnv: string;
    private readonly envFilePath: string;
    private envVarValues: any;

    constructor(envPath: string, fileNameResolver: (env: string) => string) {
        super();

        if (!process.env.NODE_ENV)
            throw new ClaireError(Errors.system.NODE_ENV_NOT_SET);

        this.currentEnv = process.env.NODE_ENV;
        this.envFilePath = path.join(envPath, fileNameResolver(this.currentEnv));
    }

    public load<T>(templateClass: new(...args: any[]) => T): T {

        if (!this.envVarValues) {
            this.envVarValues = {};
            if (!templateClass.prototype || !templateClass.prototype[ENV_VAR_PROTOTYPE_KEY]) {
                throw new ClaireError(Errors.system.INVALID_ENV_METADATA, this.envFilePath);
            }

            let env;
            try {
                env = require(this.envFilePath);
            } catch (err) {
                throw new ClaireError(Errors.system.FILE_ACCESS_ERROR, this.envFilePath);
            }
            //-- list all properties of env
            let keys = Object.keys(env);
            if (!keys || !keys.length) {
                throw new ClaireError(Errors.system.INVALID_ENV_FILE, this.envFilePath);
            }

            let envVars = env[keys[0]];
            let envKeys = Object.keys(envVars);

            //-- set values from env file
            envKeys.forEach((key) => {
                this.envVarValues[key] = envVars[key];
            });

            //-- set value from environment variables
            let allKeys = Object.keys(templateClass.prototype[ENV_VAR_PROTOTYPE_KEY]);
            allKeys.forEach((key) => {
                let envValue = process.env[key];
                if (envValue !== undefined) {
                    switch (templateClass.prototype[ENV_VAR_PROTOTYPE_KEY][key]) {
                        case "Number":
                            let numberValue = Number(envValue);
                            if (isNaN(numberValue)) {
                                throw new ClaireError(Errors.system.INVALID_NUMERIC_VALUE, `${key}: ${envValue}`);
                            }
                            this.envVarValues[key] = numberValue;
                            break;
                        case "Boolean":
                            this.envVarValues[key] = envValue === "true";
                            break;
                        case "String":
                            this.envVarValues[key] = envValue;
                            break;
                    }
                }
            });

            //-- check again for missing value
            allKeys.forEach((key) => {
                if (this.envVarValues[key] === undefined) {
                    throw new ClaireError(Errors.system.ENV_VAR_NOT_SET, key);
                }
            });

        }

        return this.envVarValues as T;
    }

    public getCurrentEnv(): string {
        return this.currentEnv;
    }

}
