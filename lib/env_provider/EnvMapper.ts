import {ClaireError} from "..";
import {Errors} from "../system/Errors";

export const ENV_VAR_PROTOTYPE_KEY = "ENV_VAR_PROTOTYPE_KEY";

const initPrototype = (prototype: any) => {
    if (!prototype[ENV_VAR_PROTOTYPE_KEY]) {
        prototype[ENV_VAR_PROTOTYPE_KEY] = {};
    }
    return prototype[ENV_VAR_PROTOTYPE_KEY];
};

export const EnvTemplate = () => {
    return <T extends { new(...args: any[]): {} }>(constructor: T) => {
        initPrototype(constructor.prototype);
        return constructor;
    };
};

export const EnvVar = () => {
    return (prototype: any, propertyKey: string) => {
        let envVarPrototype = initPrototype(prototype);
        let t = Reflect.getMetadata("design:type", prototype, propertyKey);
        switch (t.name) {
            case "Boolean":
            case "Number":
            case "String":
                envVarPrototype[propertyKey] = t.name;
                break;
            default:
                throw new ClaireError(Errors.system.ENV_VAR_TYPE_NOT_SUPPORTED, propertyKey + ":" + t.name);
        }
    }
};
