import {IEnvProvider} from "./IEnvProvider";

export abstract class AbstractEnvProvider implements IEnvProvider {

    protected constructor() {
    }

    public abstract load<T>(template: new(...args: any[]) => T): T;

    public abstract getCurrentEnv(): string;

}
