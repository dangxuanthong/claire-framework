export class ClaireError extends Error {

    public name: string;
    public message: any;

    public constructor(code: string, message?: any) {
        super();
        Object.setPrototypeOf(this, ClaireError.prototype);
        this.name = code;
        this.message = message;
    }

}