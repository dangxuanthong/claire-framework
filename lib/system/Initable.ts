import {IAppContext} from "./IAppContext";

export interface Initable {

    init(appContext: IAppContext): Promise<void>

    stop(): Promise<void>;

}
