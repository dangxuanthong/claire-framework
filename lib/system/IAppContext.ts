import {
    ControllerHandlerMetadata,
    ICacheAdapter,
    IDatabaseAdapter,
    ILogger,
    IMessagingAdapter,
    IServiceProvider,
    ISocketProvider, SocketChannelHandler
} from "..";

export interface IAppContext {

    getLogger(): ILogger;

    getServiceProvider(): IServiceProvider;

    getSocketProvider(): ISocketProvider;

    getHttpEndpoints(): ControllerHandlerMetadata[];

    getSocketEndpoints(): SocketChannelHandler[];

    getDatabaseAdapter(): IDatabaseAdapter;

    getCacheAdapter(): ICacheAdapter;

    getMessagingAdapter(): IMessagingAdapter;

    getHttpMountPoint(): string;

    getSocketMountPoint(): string;
}
