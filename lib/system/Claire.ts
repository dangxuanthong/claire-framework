import http from "http";
import {
    AbstractCacheAdapter,
    AbstractDatabaseAdapter,
    AbstractService,
    IAppContext,
    AbstractMessagingAdapter,
} from "..";

import {Initable} from "./Initable";
import {AbstractRequestHandler} from "../controller/AbstractRequestHandler";
import {AbstractAppContext} from "./AbstractAppContext";
import {Utils} from "./Utils";
import {version} from "../../package.json";

export class Claire {

    private readonly appContext: AbstractAppContext;
    private readonly cacheAdapter?: AbstractCacheAdapter;
    private readonly dataAdapter?: AbstractDatabaseAdapter;
    private readonly services: AbstractService[];
    private readonly requestHandlers: AbstractRequestHandler<any>[];
    private readonly messagingAdapter?: AbstractMessagingAdapter;
    private readonly bootstrapper?: Initable;

    private initables: Initable[];

    public constructor(
        appContext: AbstractAppContext,
        cacheAdapter: AbstractCacheAdapter | undefined,
        dataAdapter: AbstractDatabaseAdapter | undefined,
        services: AbstractService[],
        requestHandlers: (AbstractRequestHandler<any> | undefined)[],
        messagingAdapter?: AbstractMessagingAdapter,
        bootstrapper?: Initable,
    ) {
        this.appContext = appContext;
        this.cacheAdapter = cacheAdapter;
        this.dataAdapter = dataAdapter;
        this.services = services;
        this.requestHandlers = Utils.getCleanArray(requestHandlers);
        this.messagingAdapter = messagingAdapter;
        this.bootstrapper = bootstrapper;
        this.initables = [];
    }

    public async stop() {
        let logger = this.appContext.getLogger();

        for (let i = this.initables.length - 1; i >= 0; i--) {
            await this.initables[i].stop();
        }

        logger.log("Server stopped.");
        process.exit();
    }

    public async start(): Promise<IAppContext> {
        let logger = this.appContext.getLogger();

        logger.log(`Using claire-framework@${version}`);

        //-- handle exceptions
        process.on('SIGTERM', () => {
            logger.warn("SIGTERM interrupt signal");
            return this.stop();
        });

        process.on('SIGINT', () => {
            logger.warn("SIGINT interrupt signal");
            return this.stop();
        });

        process.on('uncaughtException', (err: any) => {
            logger.error('uncaughtException', err.stack);
            return this.stop();
        });

        process.on('unhandledRejection', (reason: any, p: any) => {
            logger.error('unhandledRejection', reason, p);
            return this.stop();
        });


        //-- messaging adapter
        if (this.messagingAdapter) {
            this.initables.push(this.messagingAdapter);
        }

        //-- cache adapter
        if (this.cacheAdapter) {
            this.initables.push(this.cacheAdapter);
        }

        //-- init models using the provided data adapter
        if (this.dataAdapter) {
            this.initables.push(this.dataAdapter);
        }

        //-- init services
        if (this.services) {
            this.services.forEach((service) => {
                this.initables.push(service);
            });
        }

        //-- init middleware
        this.requestHandlers.forEach((handler) => {
            this.initables.push(handler);
        });

        //-- init services and other initables
        for (let i = 0; i < this.initables.length; i++) {
            await this.initables[i].init(this.appContext);
        }

        //-- call bootstrapper
        if (this.bootstrapper) {
            await this.bootstrapper.init(this.appContext);
        }

        //-- get all ports
        let handlerMap = this.requestHandlers.reduce<{ listenPort: number, handlers: AbstractRequestHandler<any>[] }[]>((accumulator, handler) => {
            let index = accumulator.findIndex((map) => map.listenPort === handler.listenPort);
            if (index < 0) {
                accumulator.push({
                    listenPort: handler.listenPort,
                    handlers: [handler]
                });
            } else {
                accumulator[index].handlers.push(handler);
            }
            return accumulator;
        }, []);


        //-- for each port create one server
        let servers = handlerMap.map((map) => {
            let server = http.createServer();
            map.handlers.forEach((handler) => {
                server = handler.configure(server);
            });
            return new Promise((resolve, reject) => {
                try {
                    server.listen(map.listenPort, () => {
                        return resolve();
                    });
                } catch (err) {
                    reject(err);
                }
            });
        });

        await Promise.all(servers);
        return this.appContext;
    }
}
