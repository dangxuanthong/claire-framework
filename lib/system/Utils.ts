export class Utils {

    public static getCleanArray<T extends any>(array: (T | undefined)[]): T[] {
        let result: T[] = [];
        array.forEach((item) => {
            if (item !== undefined) {
                result.push(item);
            }
        });
        return result;
    }

    public static getCleanObject(obj: any): any {
        if (!obj) {
            return obj;
        }
        let result: any = {};
        Object.keys(obj).forEach((key) => {
            if (obj[key] !== undefined) {
                result[key] = obj[key];
            }
        });
        return result;
    }

    public static escapeRegExp(regex: string) {
        // $& means the whole matched string, from jQuery official document
        return regex.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }

    public static isNullOrUndefined(value: any): boolean {
        return value === null || value === undefined;
    }
}
