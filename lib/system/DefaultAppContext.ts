import {AbstractAppContext} from "./AbstractAppContext";
import {Errors} from "./Errors";
import {AbstractServiceProvider} from "../service/AbstractServiceProvider";
import {
    AbstractCacheAdapter,
    AbstractDatabaseAdapter,
    AbstractLogger,
    AbstractMessagingAdapter,
    ClaireError, ControllerHandlerMetadata,
    ICacheAdapter,
    IDatabaseAdapter, IEnvProvider, ILogger,
    IMessagingAdapter,
    IServiceProvider,
    ISocketProvider, SocketChannelHandler,
} from "..";
import {AbstractHttpRequestHandler} from "../controller/http_handler/AbstractHttpRequestHandler";
import {AbstractSocketRequestHandler} from "../controller/socket_handler/AbstractSocketRequestHandler";
import {DefaultSocketProvider} from "../controller/socket_handler/DefaultSocketProvider";

export class DefaultAppContext extends AbstractAppContext {

    private socketProvider: DefaultSocketProvider;
    private envProvider?: IEnvProvider;
    private logger: ILogger;
    private databaseAdapter?: AbstractDatabaseAdapter;
    private cacheAdapter?: AbstractCacheAdapter;
    private serviceProvider?: AbstractServiceProvider;
    private httpRequestHandler: AbstractHttpRequestHandler;
    private socketRequestHandler: AbstractSocketRequestHandler;
    private messagingAdapter?: AbstractMessagingAdapter;

    private httpEndpoints: ControllerHandlerMetadata[];
    private socketEndpoints: SocketChannelHandler[];

    public constructor() {
        super();
        //-- default to console log
        this.logger = console;
    }

    //-- getters
    public getEnvProvider(): IEnvProvider {
        if (!this.envProvider) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getEnvProvider");
        }
        return this.envProvider;
    }

    public getLogger(): ILogger {
        return this.logger;
    }

    public getDatabaseAdapter(): IDatabaseAdapter {
        if (!this.databaseAdapter) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getDatabaseAdapter");
        }
        return this.databaseAdapter;
    }

    public getServiceProvider(): IServiceProvider {
        if (!this.serviceProvider) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getServiceProvider");
        }
        return this.serviceProvider;
    }

    public getCacheAdapter(): ICacheAdapter {
        if (!this.cacheAdapter) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getCacheAdapter");
        }
        return this.cacheAdapter;
    }

    public getSocketProvider(): ISocketProvider {
        if (!this.socketProvider) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getSocketProvider");
        }
        return this.socketProvider;
    }


    public getMessagingAdapter(): IMessagingAdapter {
        if (!this.messagingAdapter) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getMessagingAdapter");
        }
        return this.messagingAdapter;
    }

    public getHttpEndpoints(): ControllerHandlerMetadata[] {
        if (!this.httpRequestHandler) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getHttpEndpoints");
        }
        if (!this.httpEndpoints) {
            this.httpEndpoints = [];
            this.httpRequestHandler.controllers.forEach(c => {
                let handlers = c.getHandlers();
                //-- find index
                handlers.forEach(handler => {
                    let matchedIndex = this.httpEndpoints.findIndex(h => h.url === handler.url && h.method === handler.method);
                    if (matchedIndex >= 0) {
                        //-- check is override
                        if (!this.httpEndpoints[matchedIndex].override && this.httpEndpoints[matchedIndex].method !== undefined && this.httpEndpoints[matchedIndex].url !== undefined) {
                            this.logger && this.logger.warn("Implicit HTTP route override, overrider:", this.httpEndpoints[matchedIndex].method, this.httpEndpoints[matchedIndex].url, this.httpEndpoints[matchedIndex].handler);
                        }
                    }
                    this.httpEndpoints.push(handler);
                });
            });
        }
        return this.httpEndpoints;
    }

    public getSocketEndpoints(): SocketChannelHandler[] {
        if (!this.socketRequestHandler) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getSocketEndpoints");
        }
        if (!this.socketEndpoints) {
            let result: SocketChannelHandler[] = [];
            for (let i = this.socketRequestHandler.controllers.length - 1; i >= 0; i--) {
                let controller = this.socketRequestHandler.controllers[i];
                let handlers = controller.getHandlers();
                for (let j = handlers.length - 1; j >= 0; j--) {
                    let handler = handlers[j];
                    //-- check if this handler had been mounted
                    let existed = result.findIndex((h) => h.channel === handler.channel);
                    if (existed >= 0) {
                        result[existed] = handler;
                    } else {
                        result.push(handler);
                    }
                }
            }
            this.socketEndpoints = result;
        }
        return this.socketEndpoints;
    }

    public getHttpMountPoint(): string {
        if (!this.httpRequestHandler) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getHttpMountPoint");
        }
        return this.httpRequestHandler.mountUrl;
    }

    public getSocketMountPoint(): string {
        if (!this.socketRequestHandler) {
            throw new ClaireError(Errors.system.RESOURCE_NOT_SET, "getSocketMountPoint");
        }
        return this.socketRequestHandler.mountUrl;
    }

    //-- setters

    public setLogger(logger: AbstractLogger) {
        this.logger = logger || console;
    }

    public setServiceProvider(serviceProvider: AbstractServiceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public setCacheAdapter(cacheAdapter: AbstractCacheAdapter) {
        this.cacheAdapter = cacheAdapter;
    }

    public setMessagingAdapter(adapter: AbstractMessagingAdapter): void {
        this.messagingAdapter = adapter;
    }

    public setDatabaseAdapter(adapter: AbstractDatabaseAdapter): void {
        this.databaseAdapter = adapter;
    }

    public setHttpRequestHandler(handler: AbstractHttpRequestHandler): void {
        this.httpRequestHandler = handler;
    }

    public setSocketRequestHandler(handler: AbstractSocketRequestHandler): void {
        this.socketRequestHandler = handler;
        this.socketProvider = new DefaultSocketProvider(handler);
    }
}
