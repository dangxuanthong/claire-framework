import {Claire} from "./Claire";

import {AbstractAppContext} from "./AbstractAppContext";
import {DefaultAppContext} from "./DefaultAppContext";
import {DefaultServiceProvider} from "../service/DefaultServiceProvider";
import {Initable} from "./Initable";
import {AbstractLogger} from "..";
import {AbstractCacheAdapter} from "..";
import {AbstractDatabaseAdapter} from "..";
import {AbstractService} from "..";
import {AbstractMessagingAdapter} from "..";
import {AbstractHttpRequestHandler} from "../controller/http_handler/AbstractHttpRequestHandler";
import {AbstractSocketRequestHandler} from "../controller/socket_handler/AbstractSocketRequestHandler";
import {Utils} from "./Utils";

export class ClaireBuilder {

    private readonly appContext: AbstractAppContext;

    private logger?: AbstractLogger;
    private cacheAdapter?: AbstractCacheAdapter;
    private dataAdapter?: AbstractDatabaseAdapter;
    private services: AbstractService[];
    private httpRequestHandler?: AbstractHttpRequestHandler;
    private socketRequestHandler?: AbstractSocketRequestHandler;
    private messagingAdapter?: AbstractMessagingAdapter;
    private bootstrapper?: Initable;

    public constructor() {
        this.appContext = new DefaultAppContext();
        this.services = [];
    }

    public setLogger(logger: AbstractLogger): ClaireBuilder {
        this.logger = logger;
        this.appContext.setLogger(this.logger);
        return this;
    }

    public setDatabaseAdapter(databaseAdapter: AbstractDatabaseAdapter): ClaireBuilder {
        this.dataAdapter = databaseAdapter;
        this.appContext.setDatabaseAdapter(this.dataAdapter);
        return this;
    }

    public setCacheAdapter(cacheAdapter: AbstractCacheAdapter): ClaireBuilder {
        this.cacheAdapter = cacheAdapter;
        this.appContext.setCacheAdapter(this.cacheAdapter);
        return this;
    }

    public setServices(services: (AbstractService | undefined)[]): ClaireBuilder {
        this.services = Utils.getCleanArray(services);
        this.appContext.setServiceProvider(new DefaultServiceProvider(this.services));
        return this;
    }

    public setSocketRequestHandler(socketRequestHandler: AbstractSocketRequestHandler | undefined): ClaireBuilder {
        this.socketRequestHandler = socketRequestHandler;
        if (this.socketRequestHandler) {
            this.appContext.setSocketRequestHandler(this.socketRequestHandler);
        }
        return this;
    }

    public setHttpRequestHandler(httpRequestHandler: AbstractHttpRequestHandler | undefined): ClaireBuilder {
        this.httpRequestHandler = httpRequestHandler;
        if (this.httpRequestHandler) {
            this.appContext.setHttpRequestHandler(this.httpRequestHandler);
        }
        return this;
    }

    public setMessagingAdapter(messagingAdapter: AbstractMessagingAdapter): ClaireBuilder {
        this.messagingAdapter = messagingAdapter;
        this.appContext.setMessagingAdapter(this.messagingAdapter);
        return this;
    }

    public setBootstrapper(bootstrapper: Initable): ClaireBuilder {
        this.bootstrapper = bootstrapper;
        return this;
    }

    public build(): Claire {
        return new Claire(
            this.appContext,
            this.cacheAdapter,
            this.dataAdapter,
            this.services,
            [this.httpRequestHandler, this.socketRequestHandler],
            this.messagingAdapter,
            this.bootstrapper,
        );
    }
}
