import {IAppContext} from "./IAppContext";

import {
    AbstractCacheAdapter,
    AbstractDatabaseAdapter, AbstractMessagingAdapter, ControllerHandlerMetadata,
    ICacheAdapter,
    IDatabaseAdapter,
    ILogger,
    IMessagingAdapter,
    IServiceProvider,
    ISocketProvider, SocketChannelHandler
} from "..";
import {AbstractServiceProvider} from "../service/AbstractServiceProvider";
import {AbstractHttpRequestHandler} from "../controller/http_handler/AbstractHttpRequestHandler";
import {AbstractSocketRequestHandler} from "../controller/socket_handler/AbstractSocketRequestHandler";

export abstract class AbstractAppContext implements IAppContext {

    protected constructor() {

    }

    //-- getters

    public abstract getLogger(): ILogger;

    public abstract getCacheAdapter(): ICacheAdapter;

    public abstract getServiceProvider(): IServiceProvider;

    public abstract getSocketProvider(): ISocketProvider;

    public abstract getMessagingAdapter(): IMessagingAdapter;

    public abstract getDatabaseAdapter(): IDatabaseAdapter;

    public abstract getHttpMountPoint(): string;

    public abstract getSocketMountPoint(): string;

    public abstract getHttpEndpoints(): ControllerHandlerMetadata[];

    public abstract getSocketEndpoints(): SocketChannelHandler[];


    //-- setters

    public abstract setLogger(logger: ILogger): void;

    public abstract setDatabaseAdapter(databaseAdapter: AbstractDatabaseAdapter): void;

    public abstract setServiceProvider(serviceProvider: AbstractServiceProvider): void;

    public abstract setCacheAdapter(cacheAdapter: AbstractCacheAdapter): void;

    public abstract setHttpRequestHandler(handler: AbstractHttpRequestHandler): void;

    public abstract setSocketRequestHandler(handler: AbstractSocketRequestHandler): void;

    public abstract setMessagingAdapter(adapter: AbstractMessagingAdapter): void;


}
