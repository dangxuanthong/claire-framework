/**
 * These data types help the presentational layer to betting parsing and validating the data.
 */
export enum DataType {
    /**
     * Boolean value.
     */
    BOOL,

    /**
     * Numerical integer value.
     */
    INTEGER,

    /**
     * Numerical floating point value.
     */
    FLOAT,

    /**
     * String value.
     */
    STRING,

    /**
     * Text is to support string with unbounded length in MySQL.
     */
    TEXT,

    /**
     * Array of any type.
     */
    ARRAY,
}

export class FieldMetaData {

    //-- data transfer layer
    /**
     * The name of the field used in business logic (controllers, services).
     */
    public fieldLogicName?: string;

    /**
     * Mark the field as auto-generated. This field cannot be manually supplied in the creation process (POST create).
     */
    public isAutoGen?: boolean;

    /**
     * The data type of the field. This will help in case the type inferring does not finely work.
     */
    public dataType?: DataType;

    //-- data access layer
    /**
     * Name of the field stored in database.
     */
    public fieldDataName?: string;

    /**
     * Whether the field can be null.
     */
    public nullable?: boolean;

    /**
     * Whether the field is primary key. Primary key cannot be null and will throw error if not correctly configured.
     */
    public isPrimaryKey?: boolean;

    /**
     * Whether this primary key is auto incremental.
     */
    public isAutoIncrement?: boolean;

    /**
     * Whether the field is a foreign key to an other table.
     * Foreign key can not be at the same time primary key, we suggest using surrogate primary key instead.
     */
    public isForeignKey?: boolean;

    /**
     * Name of the model being referred in case this field is a foreign key.
     */
    public referModel?: string;

    /**
     * Whether the value of this field is unique. Sparse is always true.
     */
    public isUnique?: boolean;

    /**
     * The default value of the field.
     */
    public defaultValue?: string | number;

}
