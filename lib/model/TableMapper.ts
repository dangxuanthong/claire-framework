import {Charset, ModelMetaData} from "./ModelMetaData";
import {DataType, FieldMetaData} from "./FieldMetaData";
import {AbstractModel} from "./AbstractModel";

export const MODEL_PROTOTYPE_METADATA_FIELD = "model_prototype";

/**
 * Init the model prototype of the class if not exist.
 * @param prototype class object prototype.
 */
const init_class_sql_prototype = (prototype: any): ModelMetaData => {
    if (!prototype[MODEL_PROTOTYPE_METADATA_FIELD]) {
        prototype[MODEL_PROTOTYPE_METADATA_FIELD] = new ModelMetaData();
    }
    return prototype[MODEL_PROTOTYPE_METADATA_FIELD];
};

/**
 * Init the logic field of the class if not exist or append properties to the existed field.
 * @param prototype class object prototype.
 * @param propertyKey logic name of the field.
 * @param addedProperties the overriding properties.
 */
const check_for_field = (prototype: any, propertyKey: string, addedProperties: Partial<FieldMetaData>): FieldMetaData => {
    let meta = init_class_sql_prototype(prototype);
    let field = meta.getFieldByLogicName(propertyKey);
    if (!field) {
        field = new FieldMetaData();
        field.fieldLogicName = propertyKey;
        field.fieldDataName = propertyKey;
        let t = Reflect.getMetadata("design:type", prototype, propertyKey);
        if (t) {
            switch (t.name) {
                case "Boolean":
                    field.dataType = DataType.BOOL;
                    break;
                case "Number":
                    field.dataType = DataType.INTEGER;
                    break;
                case "String":
                    field.dataType = DataType.STRING;
                    break;
                case "Array":
                    field.dataType = DataType.ARRAY;
                    break;
                default:
                    field.dataType = DataType.TEXT;
            }
        }
        meta.addField(field);
    }
    Object.assign(field, addedProperties);
    return field;
};

/**
 * Mark a model as data table, which will be used for database adapter.
 * @param tableName name of the table in database.
 * @param charset encoding charset.
 */
export const DataTable = (tableName?: string, charset?: Charset) => {
    return <T extends { new(...args: any[]): {} }>(constructor: T) => {
        let modelMetaData = init_class_sql_prototype(constructor.prototype);
        modelMetaData.modelName = constructor.name;
        modelMetaData.tableName = tableName || modelMetaData.modelName.toLowerCase();
        modelMetaData.charset = charset || Charset.UTF8;
        return constructor;
    };
};

/**
 * Mark a field of the model having a corresponding logic 'getter/setter' name.
 * @param fieldLogicName the logic name of the field
 */
export const GetSet = (fieldLogicName: string) => {
    return (prototype: any, propertyKey: string): void => {
        let meta = init_class_sql_prototype(prototype);
        let field = meta.getFieldByLogicName(propertyKey);
        if (!field) {
            check_for_field(prototype, propertyKey, {fieldLogicName});
        } else {
            //-- found the field
            field.fieldLogicName = fieldLogicName;
        }
    };
};

/**
 * Mark a field of the model as the data field, which will be used to store the data.
 * @param fieldDataName name of the field in the database table.
 * @param dataType explicit data type to support the database engine.
 */
export const DataField = (fieldDataName?: string, dataType?: DataType) => {
    return (prototype: any, propertyKey: string) => {
        let meta = init_class_sql_prototype(prototype);
        let field = meta.getFieldByLogicName(propertyKey);
        if (!field) {
            //-- in case not found, maybe the GetSet had changed fieldLogicName to the fieldDataName
            field = meta.getFieldByDataName(propertyKey);
            if (!field) {
                //-- not found, create the field
                let addedProperties: Partial<FieldMetaData> = {};
                if (fieldDataName) {
                    addedProperties.fieldDataName = fieldDataName;
                }
                check_for_field(prototype, propertyKey, addedProperties);
            } else {
                //-- found the field create by GetSet, only change the fieldDataName
                field.fieldDataName = fieldDataName;
            }
        }
    };
};

/**
 * Mark a field of the model as primary key. If autoInc is true, the field is auto incremental.
 * @param autoInc whether the key is auto-incremented.
 */
export const PrimaryKey = (autoInc: boolean) => {
    return (prototype: any, propertyKey: string) => {
        check_for_field(prototype, propertyKey, {isPrimaryKey: true, isAutoIncrement: autoInc, nullable: false});
    };
};

/**
 * Mark a field of the model as foreign key. The referred model must be supplied.
 * @param model the model to refer of which this field is primary key.
 */
export const ForeignKey = (model: { new(...args: any[]): AbstractModel }) => {
    return (prototype: any, propertyKey: string) => {
        check_for_field(prototype, propertyKey, {isForeignKey: true, referModel: model && model.name});
    };
};

/**
 * Mark the field with unique constraint.
 */
export const Unique = () => {
    return (prototype: any, propertyKey: string) => {
        check_for_field(prototype, propertyKey, {isUnique: true});
    };
};

/**
 * Mark the field with can-be-null constraint.
 */
export const Nullable = () => {
    return (prototype: any, propertyKey: string) => {
        check_for_field(prototype, propertyKey, {nullable: true});
    };
};

/**
 * Default value of the field.
 * @param value the default value.
 */
export const Default = (value: any) => {
    return (prototype: any, propertyKey: string) => {
        check_for_field(prototype, propertyKey, {defaultValue: value});
    };
};

/**
 * Mark the field as auto-generated. This field can be populate from the business logic but from the user.
 */
export const AutoGen = () => {
    return (prototype: any, propertyKey: string) => {
        check_for_field(prototype, propertyKey, {isAutoGen: true});
    };
};
