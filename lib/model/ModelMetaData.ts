import {DataType, FieldMetaData} from "./FieldMetaData";
import {Errors} from "../system/Errors";
import {ClaireError} from "..";

export enum Charset {
    UTF8,
    ASCII,
}

export class ModelMetaData {

    private readonly fields: FieldMetaData[];
    public modelName?: string;
    public tableName?: string;
    public charset?: Charset;

    public constructor() {
        this.fields = [];
    }

    public getPrimaryKey(): FieldMetaData {
        return this.fields.find(f => f.isPrimaryKey === true)!;
    }

    public getFields(): FieldMetaData[] {
        return this.fields;
    }

    public addField(field: FieldMetaData): void {
        this.fields.push(field);
    }

    public getFieldByLogicName(fieldLogicName: string): FieldMetaData | undefined {
        return this.fields.find((f) => f.fieldLogicName === fieldLogicName);
    }

    public getFieldByDataName(fieldDataName: string): FieldMetaData | undefined {
        return this.fields.find((f) => f.fieldDataName === fieldDataName);
    }

    public getDefaultLogicValues<T>(): Partial<T> {
        let result: Partial<T> = {};
        this.fields.forEach((field) => {
            if (field.defaultValue !== undefined) {
                // @ts-ignore
                result[field.fieldLogicName!] = field.defaultValue;
            }
        });
        return result;
    }

    public assertValid(): void {

        let isValid = true;
        if (!this.tableName) {
            isValid = false;
        }
        if (this.charset === undefined) {
            isValid = false;
        }

        let primaryKey = this.getPrimaryKey();
        if (!primaryKey) {
            throw new ClaireError(Errors.database.TABLE_HAS_NO_PRIMARY_KEY, this.modelName);
        }
        if (![DataType.STRING, DataType.INTEGER].includes(primaryKey.dataType!)) {
            throw new ClaireError(Errors.database.UNSUPPORTED_PRIMARY_KEY_DATA_TYPE, this.modelName);
        }
        if (primaryKey.isAutoGen) {
            throw new ClaireError(Errors.database.NO_SKIP_PRIMARY_KEY, this.modelName);
        }

        isValid = isValid && this.fields.every((f) => {
            let isValid = true;
            if (f.fieldDataName === undefined || f.dataType === undefined) {
                isValid = false;
            }
            return isValid;
        });
        if (!isValid) {
            throw new ClaireError(Errors.database.INVALID_MODEL_METADATA, this.modelName);
        }
    }

    public static isNotEmptyMetadata = (meta: ModelMetaData): boolean => {
        return meta.tableName !== undefined || meta.charset !== undefined
            || meta.getFields().some((f) => f.fieldDataName !== undefined || f.isPrimaryKey !== undefined
                || f.isForeignKey !== undefined || f.isAutoIncrement !== undefined || f.isUnique !== undefined
                || f.referModel !== undefined || f.dataType !== undefined || f.nullable !== undefined || f.defaultValue !== undefined
            );
    };

    public static dependencySort(modelMetaData: ModelMetaData[]): ModelMetaData[] {

        let result: ModelMetaData[] = [];

        let metaDataArray = modelMetaData.map((tableMetaData) => ({
            metaData: tableMetaData,
            dependencies: new Array<ModelMetaData>()
        }));


        //-- first find all the dependencies of current table and add to the dependencies array
        for (let i = 0; i < metaDataArray.length; i++) {
            let metaData = metaDataArray[i];
            //-- check for dependencies
            let fields = metaData.metaData.getFields();
            for (let j = 0; j < fields.length; j++) {
                let field = fields[j];
                if (field.isForeignKey) {
                    if (!!field.referModel) {
                        let referredModel = modelMetaData.find(m => m.modelName === field.referModel);
                        if (!!referredModel && referredModel.modelName) {
                            if (referredModel.getPrimaryKey().dataType !== field.dataType) {
                                throw new ClaireError(Errors.database.FOREIGN_KEY_TYPE_MISMATCHED, metaData.metaData.modelName);
                            }
                            metaData.dependencies.push(referredModel);
                        } else {
                            throw new ClaireError(Errors.database.CIRCULAR_DEPENDENCY_DETECTED, {
                                modelName: metaData.metaData.modelName,
                                fieldName: field.fieldDataName,
                            });
                        }
                    } else {
                        throw new ClaireError(Errors.database.CIRCULAR_DEPENDENCY_DETECTED, {
                            modelName: metaData.metaData.modelName,
                            fieldName: field.fieldDataName,
                        });
                    }
                }
            }
        }

        //-- add the dependant to the result array first, then add the dependencies. If dependencies are found to be before
        //-- the dependant in the result array, circular happens
        for (let i = 0; i < metaDataArray.length; i++) {
            let currentElement = metaDataArray[i];
            let currentElementIndex = result.findIndex(e => e === currentElement.metaData);
            if (currentElementIndex < 0) {
                //-- push the currentElement
                result = [
                    ...result, currentElement.metaData
                ];
                currentElementIndex = result.length - 1;
            }

            //-- check dependencies index
            for (let i = 0; i < currentElement.dependencies.length; i++) {
                let dependencyIndex = result.findIndex(e => e === currentElement.dependencies[i]);
                if (dependencyIndex < 0) {
                    //-- push dependency just before
                    result = [
                        ...result.slice(0, currentElementIndex),
                        currentElement.dependencies[i],
                        ...result.slice(currentElementIndex),
                    ];
                } else {
                    //-- check for circular dependency
                    currentElementIndex = result.findIndex(e => e === currentElement.metaData);
                    if (dependencyIndex > currentElementIndex)
                        throw new ClaireError(Errors.database.CIRCULAR_DEPENDENCY_DETECTED, {
                            target1: currentElement.metaData.modelName,
                            target2: result[dependencyIndex].modelName,
                        });

                }
            }
        }

        return result;
    }

}
